#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
//
// Konami Code added by BulletWare (Paul Shields)
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
#endregion

namespace BulletHeli
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class LevelSelectScreen : MenuScreen
    {
        #region Initialization

        //Music to play at this menu
        string musicCue = "MenuMusic";
        bool konamiCode = false;

        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public LevelSelectScreen()
            : base("Level Select")
        {
            // Create our menu entries.
            MenuEntry level1Entry = new MenuEntry("Level 1");
            MenuEntry level2Entry = new MenuEntry("Level 2");
            MenuEntry level3Entry = new MenuEntry("Level 3");
            MenuEntry exitMenuEntry = new MenuEntry("Exit");

            // Hook up menu event handlers.
            level1Entry.Selected += new EventHandler<PlayerIndexEventArgs>(level1Entry_Selected);
            level2Entry.Selected += new EventHandler<PlayerIndexEventArgs>(level2Entry_Selected);
            level3Entry.Selected += new EventHandler<PlayerIndexEventArgs>(level3Entry_Selected);
            exitMenuEntry.Selected += OnCancel;

            // Add entries to the menu.
            MenuEntries.Add(level1Entry);
            MenuEntries.Add(level2Entry);
            MenuEntries.Add(level3Entry);
            MenuEntries.Add(exitMenuEntry);
        }

        public override void LoadContent()
        {
            //Play menu music
            ScreenManager.Audio.Play(musicCue);
        }


        #endregion

        #region Handle Input

        void level1Entry_Selected(object sender, PlayerIndexEventArgs e)
        {
            if (konamiCode)
                ScreenManager.Audio.Stop("Alt" + musicCue);
            else
                ScreenManager.Audio.Stop(musicCue);

            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new BackgroundScreen(),
                               new Username(1, konamiCode));
        }

        void level2Entry_Selected(object sender, PlayerIndexEventArgs e)
        {
            if (konamiCode)
                ScreenManager.Audio.Stop("Alt" + musicCue);
            else
                ScreenManager.Audio.Stop(musicCue);

            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new BackgroundScreen(),
                               new Username(2, konamiCode));
        }

        void level3Entry_Selected(object sender, PlayerIndexEventArgs e)
        {
            if (konamiCode)
                ScreenManager.Audio.Stop("Alt" + musicCue);
            else
                ScreenManager.Audio.Stop(musicCue);

            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new BackgroundScreen(),
                               new Username(3, konamiCode));
        }

        #endregion
    }
}
