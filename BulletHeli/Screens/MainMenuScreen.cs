#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
//
// Konami Code added by BulletWare (Paul Shields)
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
#endregion

namespace BulletHeli
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class MainMenuScreen : MenuScreen
    {
        #region Initialization

        //Music to play at this menu
        string musicCue = "MenuMusic";

        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public MainMenuScreen()
            : base("BulletHeli")
        {
            // Create our menu entries.
            MenuEntry playGameMenuEntry = new MenuEntry("Play Game");
            MenuEntry leaderboardMenuEntry = new MenuEntry("Leaderboard");
            MenuEntry controlMenuEntry = new MenuEntry("Controls");
            MenuEntry exitMenuEntry = new MenuEntry("Exit");

            // Hook up menu event handlers.
            playGameMenuEntry.Selected += PlayGameMenuEntrySelected;
            leaderboardMenuEntry.Selected += LeaderboardMenuEntrySelected;
            controlMenuEntry.Selected += ControlMenuEntrySelected;
            exitMenuEntry.Selected += OnCancel;

            // Add entries to the menu.
            MenuEntries.Add(playGameMenuEntry);
            MenuEntries.Add(leaderboardMenuEntry);
            MenuEntries.Add(controlMenuEntry);
            MenuEntries.Add(exitMenuEntry);
        }

        public override void LoadContent()
        {
            //Play menu music
            ScreenManager.Audio.Play(musicCue);
        }


        #endregion

        #region Handle Input

        /// <summary>
        /// Event handler for when the Play Game menu entry is selected.
        /// </summary>
        void PlayGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            if (konamiCode)
                ScreenManager.Audio.Stop("Alt" + musicCue);
            else
                ScreenManager.Audio.Stop(musicCue);

            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new BackgroundScreen(),
                               new LevelSelectScreen());
        }


        /// <summary>
        /// Event handler for when the Options menu entry is selected.
        /// </summary>
        void LeaderboardMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Audio.Stop(musicCue);
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new BackgroundScreen(),
                                                           new Leaderboard());
        }

        /// <summary>
        /// Event handler for when the coltrols menu entry is selected.
        /// </summary>
        void ControlMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Audio.Stop(musicCue);
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new BackgroundScreen(),
                                                           new Controls());
        }


        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            const string message = "Are you sure you want to leave BulletHeli?";

            MessageBoxScreen confirmExitMessageBox = new MessageBoxScreen(message);

            confirmExitMessageBox.Accepted += ConfirmExitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmExitMessageBox, playerIndex);
        }


        /// <summary>
        /// Event handler for when the user selects ok on the "are you sure
        /// you want to exit" message box.
        /// </summary>
        void ConfirmExitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Game.Exit();
        }


        #endregion

        #region Konami Code

        bool konamiCode = false;
        int konamiCodeCheck = 0;
        //bool konamiCodeButtonCheck = true;

        public override void HandleInput(InputState input)
        {
            //Look up inputs for the active player profile.
            //int playerIndex = (int)ControllingPlayer.Value;

            //Gets input states to run from
            KeyboardState keyboardState = input.CurrentKeyboardStates[0];
            GamePadState gamePadState = input.CurrentGamePadStates[0];
            KeyboardState lastKeyboardState = input.LastKeyboardStates[0];
            GamePadState lastGamePadState = input.LastGamePadStates[0];

            //Check if the gamepad has been disconnected (if originally connected)
            bool gamePadDisconnected = !gamePadState.IsConnected && input.GamePadWasConnected[1];

            #region Keyboard
            if (keyboardState.GetPressedKeys().Length > 0)
            {
                //Konami Code Check - check if the legendary cheat code has been entered
                //If yes, then it is passed into the game for whatever
                if (keyboardState.IsKeyDown(Keys.Up))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Up) && (konamiCodeCheck == 0 || konamiCodeCheck == 1))
                        konamiCodeCheck++;
                }
                else if (keyboardState.IsKeyDown(Keys.Down))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Down) && (konamiCodeCheck == 2 || konamiCodeCheck == 3))
                        konamiCodeCheck++;
                }
                else if (keyboardState.IsKeyDown(Keys.Left))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Left) && (konamiCodeCheck == 4 || konamiCodeCheck == 6))
                        konamiCodeCheck++;
                }
                else if (keyboardState.IsKeyDown(Keys.Right))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.Right) && (konamiCodeCheck == 5 || konamiCodeCheck == 7))
                        konamiCodeCheck++;
                }
                else if (keyboardState.IsKeyDown(Keys.B))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.B) && (konamiCodeCheck == 8))
                        konamiCodeCheck++;
                }
                else if (keyboardState.IsKeyDown(Keys.A))
                {
                    if (lastKeyboardState.IsKeyUp(Keys.A) && (konamiCodeCheck == 9))
                    {
                        konamiCodeCheck++;
                        konamiCode = true;

                        ScreenManager.Audio.Stop(musicCue);
                        ScreenManager.Audio.Play("Alt" + musicCue);
                    }
                }
                else
                    //Reset if done wrong
                    konamiCodeCheck = 0;
            }
            #endregion //Keyboard

            /*#region Gamepad
            if (gamePadState.IsConnected)
            {
                //Konami Code Check - check if the legendary cheat code has been entered
                //If yes, then it is passed into the game for whatever

                if (gamePadState.ThumbSticks.Left.X > 0.7 && (konamiCodeCheck == 0 || konamiCodeCheck == 1))
                {
                    konamiCodeCheck++;
                    konamiCodeButtonCheck = false;
                }
                else if (gamePadState.ThumbSticks.Left.X < 0.7 && (konamiCodeCheck == 2 || konamiCodeCheck == 3))
                {
                    konamiCodeCheck++;
                    konamiCodeButtonCheck = false;
                }
                else if (gamePadState.ThumbSticks.Left.Y > 0.7 && (konamiCodeCheck == 4 || konamiCodeCheck == 6))
                {
                    konamiCodeCheck++;
                    konamiCodeButtonCheck = false;
                }
                else if (gamePadState.ThumbSticks.Left.Y < 0.7 && (konamiCodeCheck == 5 || konamiCodeCheck == 7))
                {
                    konamiCodeCheck++;
                    konamiCodeButtonCheck = false;
                }
                else
                    konamiCodeButtonCheck = true;


                if (gamePadState.IsButtonDown(Buttons.B))
                {
                    if (lastGamePadState.IsButtonUp(Buttons.B) && (konamiCodeCheck == 2 || konamiCodeCheck == 3))
                        konamiCodeCheck++;
                }
                else if (gamePadState.IsButtonDown(Buttons.A))
                {
                    if (gamePadState.IsButtonDown(Buttons.A) && (konamiCodeCheck == 9))
                    {
                        konamiCodeCheck++;
                        konamiCode = true;

                        ScreenManager.Audio.StopCue(musicCue);
                        ScreenManager.Audio.PlayCue("Alt" + musicCue);
                    }
                }
                else
                    //Reset if done wrong
                    konamiCodeCheck = 0;

            }
            #endregion //Gamepad*/

            //Make sure menu can still update and move);
            base.HandleInput(input);
        }

        #endregion
    }
}
