﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class Lives
    {
                                                #region PROPERTIES

        //References
        private TheGame game;                                   //Save reference to the game
        private Texture2D heartTexture;                         //Heart sprite

        //Properties
        private Vector2 origLoc;                                //Location to start drawing the hearts from
        private float spacing;                                  //Space between hearts, not including width of heart (already considered in code)
        private List<Vector2> drawingLoc = new List<Vector2>(); //Locations to draw hearts at
        private int currentLives;                               //Store the current hearts we are drwing to avoid uneccessary calculation
        private float maxWidth;                                 //The max width of the textbox
        

                                                #endregion //PROPERTIES

        /*******************************************************************************************************************************/

                                                #region INITIALIZATION

        /// <summary>
        /// Generates the "Lives" UI object
        /// </summary>
        /// <param name="game">TheGame reference</param>
        /// <param name="origLoc">Location of the first heart</param>
        /// <param name="maxWidth">Maximum width the hearts can take up</param>
        /// <param name="spacing">Spacing between the hearts</param>
        public void Initialize(TheGame game, Vector2 origLoc, float maxWidth, float spacing)
        {
            //Save TheGame for referencing
            this.game = game;

            //Properties
            this.origLoc = origLoc;
            this.spacing = spacing;
            this.maxWidth = maxWidth;
            currentLives = 0;
        }

        public void LoadContent()
        {
            //Load the heart sprite
            heartTexture = game.ContentManager.Load<Texture2D>("UI/truck");
        }

                                                #endregion //INITIALIZATION

                                                #region UPDATE

        /// <summary>
        /// Updates the hearts UI whenever the number of lives has changed.
        /// under 6 will draw hearts separately
        /// over 6 will draw 1 heart with number in texts
        /// </summary>
        public void Update()
        {
            //Check if the number of lives has changed since last update
            if (currentLives != game.Player.Lives)
            {
                //Reset hearts
                currentLives = game.Player.Lives;
                drawingLoc.Clear();

                //If there are enough lives to draw them individually
                if (currentLives <= 6)
                {
                    //Recalculate the locations for the hearts
                    int line = 0;
                    int hardI = 0;
                    for (int i = 0; i < currentLives; i++)
                    {
                        //Based on the loaction of the first heart in the line, calculate the position of this individual heart
                        //Spacing, number of hearts per line are definied in initialise
                        float propYWidth = origLoc.X + ((heartTexture.Width + spacing)*hardI) + heartTexture.Width;

                        //Calculate if new line is needed
                        if (propYWidth > maxWidth)
                        {
                            line++;
                            hardI = 0;
                        }

                        //Save location of heart to draw
                        Vector2 newLoc = new Vector2();
                        newLoc.X = origLoc.X + ((heartTexture.Width + spacing)*hardI);
                        newLoc.Y = origLoc.Y + ((heartTexture.Height + spacing)*line);

                        drawingLoc.Add(newLoc);
                        hardI++;
                    }
                }
                else
                {
                    //If ther are too many hearts to drwa separately, draw 1 and the number of lives left in text form
                    Vector2 newLoc = new Vector2();
                    newLoc.X = origLoc.X;
                    newLoc.Y = origLoc.Y;
                    drawingLoc.Add(newLoc);
                }
            }
        }

        /// <summary>
        /// Draws everything required in lives UI
        /// </summary>
        public void Draw()
        {
            //Draw all hearts in list
            foreach (var position in drawingLoc)
            {
                game.SpriteBatch.Draw(heartTexture, position, heartTexture.Bounds, Color.White);
            }

            //Draw text with number of lives if required
            if (currentLives > 6)
                game.SpriteBatch.DrawString(game.GenericFont, "= " + currentLives, new Vector2(drawingLoc[0].X + heartTexture.Width + 2, drawingLoc[0].Y), Color.White);
        }


                                                #endregion //UPDATE
    }
}
