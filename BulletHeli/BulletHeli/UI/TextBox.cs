﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class TextBox
    {
                                                #region PROPERTIES

        //References
        private TheGame game;                                   //Save reference to the game
        private SpriteFont font;                                //Font used

        //Properties
        private string stringOText;                             //String of text
        private Vector2 position;                               //Position of the name
        private float maxWidth;                                 //The max width of the textbox

        //Returns
        private Vector2 stringSize;                            //Draw width and height of the string

        

                                                #endregion //PROPERTIES

                                                #region RETURNS



                                                #endregion //RETURNS

        /*******************************************************************************************************************************/

                                                #region INITIALIZATION

        /// <summary>
        /// Generates textbox.
        /// </summary>
        /// <param name="game">passes in a reference to the game</param>
        /// <param name="position">passes in the position of the textbox</param>
        /// <param name="maxWidth">dictates the maximum width of the textbox</param>
        /// <param name="stringOText">passes in the text to be put into the textbox</param>
        public void Initialize(TheGame game, Vector2 position, float maxWidth, string stringOText)
        {
            //Save TheGame for referencing
            this.game = game;

            //Load settings
            font = game.ContentManager.Load<SpriteFont>("UI/TheFont");
            this.position = position;
            this.maxWidth = maxWidth;

            //Ensure line is not greater than the width allowed
            this.stringOText = ParseText(stringOText, maxWidth);

            //Save the draw width of the string
            stringSize = font.MeasureString(this.stringOText);
        }

        public void Update(string stringOText)
        {
            //Update the string to draw and the size of it
            this.stringOText = ParseText(stringOText, maxWidth);
            stringSize = font.MeasureString(this.stringOText);
        }

        public void Update(Vector2 position)
        {
            //Update the position of the text box
            this.position = position;
        }

                                                #endregion //INITIALIZATION

                                                #region UPDATE

        public void Draw()
        {
            //Draw the text box
            game.SpriteBatch.DrawString(font, stringOText, position, Color.White);
        }


                                                #endregion //UPDATE

        /*******************************************************************************************************************************/

                                                #region PARSE TEXT

        /// <summary>
        /// Parses the text. If the text if greater than the given width, add a line break
        /// Code taken and adapted from: http://danieltian.wordpress.com/2008/12/24/xna-tutorial-typewriter-text-box-with-proper-word-wrapping-part-2/
        /// </summary>
        /// <param name="text">String of text to be parsed</param>
        /// <returns></returns>
        private String ParseText(String text, float width)
        {
            String line = String.Empty;
            String returnString = String.Empty;
            String[] wordArray = text.Split(' ');

            foreach (String word in wordArray)
            {
                if (font.MeasureString(line + word).Length() > width)
                {
                    returnString = returnString + line + '\n';
                    line = String.Empty;
                }

                line = line + word + ' ';
            }

            return returnString + line;
        }

                                                #endregion //PARSE TEXT
    }
}
