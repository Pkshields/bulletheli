﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class UIBar
    {
                                                #region PROPERTIES

        //References
        private TheGame game;                                   //Save reference to the game
        private Texture2D coloredBox;                           //1x1 coloured box to expand on

        //Properties
        private Vector2 bottomLeft;                             //bottom left point of our bar
        private bool isVertical;                                //Defines if this bar is vertically or horizontally situated
        private float otherSide;                                //Size of the unchanged side of the bar
        private float maxSideSize;                              //Define the maximum size of the moveable side
        private float maxValue;                                 //Max value for the bar (used for calculating % moveable side size)
        private Rectangle drawRect;                             //Current rect to draw
        private Color color;                                    //Color of the box

        private bool hasBorder;                                 //Decide if the bar has a border
        private Color borderColor;                              //Color of the border
        private Rectangle borderRect;                           //Drawable rect for the border
        private bool hasRed;                                    //Decide if the bar has a red background behind the bar
        private Color redColor;                                 //Color of the red background
        private Rectangle redRect;                              //Drawable rect for the red background

                                                    #endregion //PROPERTIES

        /*******************************************************************************************************************************/

                                                #region INITIALIZATION

        /// <summary>
        /// Generates two bars which act as Health and Energy
        /// </summary>
        /// <param name="game">Passes a reference to the game</param>
        /// <param name="bottomLeft">bottom left location of the bar</param>
        /// <param name="isVertical">boolean value to make a vertical bar</param>
        /// <param name="otherSide">Width or height of the bar depending if it is vertical or horizontal</param>
        /// <param name="maxSideSize">Full Height of the bar</param>
        /// <param name="maxValue">Maximum value used for calculating percentage</param>
        /// <param name="hasBorder">Boolean whether bar has a border</param>
        /// <param name="hasRed">Has a different coloured bar than the forecolour (red in this case)</param>
        /// <param name="color">Color of the bar</param>
        /// <param name="borderColor">Colour of the border (black)</param>
        /// <param name="borderWidth">Width of the black border around the bar</param>
        /// <param name="redColor">color of the red in the background</param>

        public void Initialize(TheGame game, Vector2 bottomLeft, bool isVertical, float otherSide, float maxSideSize, float maxValue, bool hasBorder, bool hasRed, Color color, Color? borderColor, int? borderWidth, Color? redColor)
        {
            //Save TheGame for referencing
            this.game = game;

            //Properties
            this.bottomLeft = bottomLeft;
            this.isVertical = isVertical;
            this.otherSide = otherSide;
            this.maxSideSize = maxSideSize;
            this.maxValue = maxValue;
            this.color = color;

            //Border and misc settings
            this.hasBorder = hasBorder;
            if (borderColor != null) this.borderColor = (Color)borderColor;
            this.hasRed = hasRed;
            if (redColor != null) this.redColor = (Color)redColor;

            int borderSize = 10;
            if (borderWidth != null) borderSize = (int)borderWidth;

            //Border and red rects all always the same, so set up here
            //TODO: See if less duplication is needed
            if (this.hasBorder && this.isVertical)
                borderRect = new Rectangle((int)(bottomLeft.X - borderSize),
                                           (int)((bottomLeft.Y - maxSideSize - borderSize)),
                                           (int)(otherSide + borderSize * 2),
                                           (int)(maxSideSize + borderSize * 2));

            else if (this.hasBorder && !this.isVertical)
                borderRect = new Rectangle((int)(bottomLeft.X - borderSize),
                                           (int)(bottomLeft.Y - otherSide - borderSize),
                                           (int)(maxSideSize + borderSize * 2),
                                           (int)(otherSide + borderSize * 2));

            if (this.hasRed && this.isVertical)
                redRect = new Rectangle((int)(bottomLeft.X),
                                        (int)((bottomLeft.Y - maxSideSize)),
                                        (int)(otherSide),
                                        (int)(maxSideSize));

            else if (this.hasRed && !this.isVertical)
                borderRect = new Rectangle((int)(bottomLeft.X),
                                           (int)(bottomLeft.Y - otherSide),
                                           (int)(maxSideSize),
                                           (int)(otherSide));
            
            //Set up colored box texture for future drawing
            coloredBox = new Texture2D(game.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            coloredBox.SetData(new[] { Color.White });
        }

                                                #endregion //INITIALIZATION

                                                #region UPDATE
        /// <summary>
        /// Updates bars to show their percentage of fill relating to value over maxvalue
        /// </summary>
        /// <param name="value">value of the maximum</param>
        public void Update(float value)
        {
            //Calculate the % moveable side size
            float size = (value / maxValue);

            //Calculate the size for the moveable side
            size = maxSideSize * size;

            //Generate the rectangle that we need to draw depending on the isVertical value
            if (isVertical)
                drawRect = new Rectangle((int) bottomLeft.X,
                                         (int) (bottomLeft.Y - size),
                                         (int) otherSide,
                                         (int) Math.Ceiling(size));
            else
                drawRect = new Rectangle((int) bottomLeft.X,
                                         (int) (bottomLeft.Y - otherSide),
                                         (int) size,
                                         (int) otherSide);
        }

        public void Draw()
        {
            //If the player demands a border, give then a 2x2 border forst (order of drawing and all that...)
            if (hasBorder)
                game.SpriteBatch.Draw(coloredBox, borderRect, borderColor);
            if (hasRed)
                game.SpriteBatch.Draw(coloredBox, redRect, redColor);

            //Draw the required rectangle
            game.SpriteBatch.Draw(coloredBox, drawRect, color);
        }
                                                
                                                #endregion //UPDATE
    }
}
