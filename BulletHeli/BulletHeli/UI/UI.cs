﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class UI
    {
                                                #region PROPERTIES

        //References
        private TheGame game;                                   //Save reference to the game

        //UI Elements
        private TextBox playerName;
        private Vector2 playerNameLoc = new Vector2(10, 10);
        private TextBox score;
        private Vector2 scoreNameLoc = new Vector2(10, 50);
        private Lives lives;
        private Vector2 livesLoc = new Vector2(10, 90);
        private UIBar health;
        private Vector2 healthLoc = new Vector2(20, 700);
        private UIBar overdrive;
        private Vector2 overdriveLoc = new Vector2(80, 700);
        private TextBox timer;
        private Vector2 timerLoc = new Vector2(1080, 600);

                                                #endregion //PROPERTIES

        /*******************************************************************************************************************************/

                                                #region INITIALIZATION

        /// <summary>
        /// Generates the graphical user interface
        /// </summary>
        /// <param name="game">passes in a reference to the game</param>
        public void Initialize(TheGame game)
        {
            //Save TheGame for referencing
            this.game = game;

            //Initalise player name
            playerName = new TextBox();
            playerName.Initialize(game, playerNameLoc, game.GameDimensions.X, "BulletHeli");

            //Score
            score = new TextBox();
            score.Initialize(game, scoreNameLoc, game.GameDimensions.X, "Score: " + game.Player.Score);

            //Hearts!
            lives = new Lives();
            lives.Initialize(game, livesLoc, game.GameDimensions.X, 3);

            //Health!!
            health = new UIBar();
            health.Initialize(game, healthLoc, true, 30, 530, game.Player.Health, true, true, Color.Green, Color.Black, 4, Color.Red);

            //Overdrive bar!
            overdrive = new UIBar();
            overdrive.Initialize(game, overdriveLoc, true, 30, 530, 100, true, true, Color.BlueViolet, Color.Black, 4, Color.DarkCyan);

            //Timer
            timer = new TextBox();
            timer.Initialize(game, timerLoc, 50, "0");

        }

        public void LoadContent()
        {
            //Load the hearts sprite in lives
            lives.LoadContent();
        }

                                                #endregion //INITIALIZATION

                                                #region UPDATE

        public void Update(GameTime gameTime)
        {
            //Update stuff in UI
            score.Update("Score: " + game.Player.Score);
            lives.Update();
            health.Update(game.Player.Health);
            overdrive.Update(100);
            //timer.Update(""+gameTime.TotalGameTime.TotalSeconds);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //Update all UI elements
            playerName.Draw();
            score.Draw();
            lives.Draw();
            health.Draw();
            overdrive.Draw();
            //timer.Draw();
        }


                                                #endregion //UPDATE
    }
}
