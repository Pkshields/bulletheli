﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class PowerupObject : CollisionObject
    {
                                                #region PROPERTIES

        //References
        private TheGame game;                                   //Save reference to the game
        private Texture2D powerupTexture;                       //Powerup texture

        //Variables
        private bool active;                                    //Determines if the powerup is active or not
        private int type;                                       //Powerup type
        private Vector2 position;                               //Powerup position
        private float speed;                                    //Speed at which the powerup will fall at
        private float gravity = 0.05f;                           //Amount at which the speed of falling will increase at
        private float maxSpeed = 4f;                            //Maximum speed at which the powerup will fall at

                                                #endregion //PROPERTIES

                                                #region RETURNS

        //Return If the powerup is active
        public bool Active
        {
            get { return active; }
        }

                                                #endregion //RETURNS

        /*******************************************************************************************************************************/

                                                #region INITIALIZATION

        public void Initialize(TheGame game, Texture2D powerupTexture, Vector2 position, int type)
        {
            //Save TheGame for referencing
            this.game = game;

            //Save powerup texture
            this.powerupTexture = powerupTexture;

            //Variables
            this.position = position;
            this.active = true;
            this.type = type;

            //Add to the collision manager
            base.Initialize(game, powerupTexture.Bounds, game.Collision, this.GetType(), null);
        }

                                                #endregion //INITIALIZATION

                                                #region UPDATE

        public void Update()
        {
            //Move the powerup down by the current speed
            position.Y += speed;

            //Use gravity to increase the speed of falling
            speed += gravity;
            if (speed > maxSpeed) speed = maxSpeed;

            //If powerup has reached the bottom of the screen, remove from powerup manager
            if (position.Y >= game.GameDimensions.Height) active = false;

            //Update collision manager
            base.Update(position);
        }

        public new void Draw()
        {
            //Draw the powerup on the screen
            game.SpriteBatch.Draw(powerupTexture, position, Color.White);
        }

                                                #endregion //UPDATE

        /*******************************************************************************************************************************/

                                                #region COLLISION MANAGEMENT

        /// <summary>
        /// When the powerup is activated by the player, apply features determined by PowerupFeatures
        /// </summary>
        /// <param name="obj"></param>
        public override void OnCollision(CollisionObject obj)
        {
            //If collision is with an enemy
            if (obj.Type.Name.Equals("Player") && active)
            {
                //Apply the powerup feature (stored in PowerupFeatures.cs)
                PowerupFeatures.RunFeature(game, type);
                active = false;
            }
        }

                                                #endregion //COLLISION MANAGEMENT
    }
}
