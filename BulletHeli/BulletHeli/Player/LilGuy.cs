﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class LilGuy : CollisionObject
    {
                                                #region PROPERTIES

        //References
        private TheGame game;                                   //Save reference to the game
        private Animation lilGuyAni;
        private Texture2D lilGuyTexture;
        private Type targetType;

        //Preferences
        private Vector2 position;                               //Current position of LilGuy
        private Gun gun;
        private float speed;                                    //Horizontal speed
        private float distanceAllowedFrom;                      //Distance from Player
        private int direction = 0;
                                                #endregion //PROPERTIES

        /*******************************************************************************************************************************/

                                                #region INITIALIZATION
        /// <summary>
        /// Initialises a Lilguy Object
        /// </summary>
        /// <param name="game">reference to the game</param>
        /// <param name="speed">speed of the lilguy</param>
        /// <param name="distanceAllowedFrom">maxdistance lilguy will allow to pursue enemies</param>
        public void Initialize(TheGame game, float speed, float distanceAllowedFrom)
        {
            //Save TheGame for referencing
            this.game = game;

            //Preferences
            this.position = new Vector2(game.Player.Position.X, game.GameDimensions.Height - 70);
            this.speed = speed;
            this.distanceAllowedFrom = distanceAllowedFrom;
            this.targetType = typeof(Enemy);
            
            //Load sprite early so we can get collision box from it
            lilGuyTexture = game.ContentManager.Load<Texture2D>("Player/uav");
            lilGuyAni = new Animation();
            lilGuyAni.Initialize(0, 1, 0, new Vector2(40, 40), null, 0, false, true);
            lilGuyAni.LoadContent(lilGuyTexture);

            //Initialise base
            base.Initialize(game, new Rectangle((int)position.X, (int)position.Y, 40, 40), game.Collision, this.GetType(), null);

            this.gun = new Gun();
            gun.Initialize(game, this, targetType, "Content/Ammo/LilGuyAmmo.txt");

        }

                                                #endregion //INITIALIZATION

                                                #region UPDATE

        /// <summary>
        /// Lilguys behavior is defined here, following player if distance from player is greater than the distance allowed, else it will pursue enemies 
        /// </summary>
        /// <param name="gameTime">passes in reference to gametime</param>
        public void Update(GameTime gameTime)
        {
            //Get distance from player to lil guy
            float distanceAway = (game.Player.Position - position).Length();
            Vector2 movingPosition = Vector2.Zero;

            //If the lil guy is outside the radius:
            if (distanceAway > distanceAllowedFrom)
            {
                //Follow Player at speed
                Vector2 test = game.Player.Position - position;
                test *= distanceAllowedFrom / distanceAway;
                test.Normalize();
                test *= speed;
                movingPosition = test;
            }
            else
            {
                //Follow nearest enemy
                //Get the enemy list from Enemy Manager
                List<Enemy> enemyList = game.EnemyManager.EnemyList;

                //If there are no enemys on screen, you can't follwo them, so dont even try
                if (enemyList.Count > 0)
                {
                    //Set the targetEnemy to the first in the list - give it something to check from later
                    Enemy targetEnemy = enemyList[0];
                    float targetEnemyLength = (position - enemyList[0].Position).Length();
                    Vector2 workingVector;  //settings to work with
                    float workingLength;    

                    //For every enemy
                    for (int i = 1; i < enemyList.Count; i++)
                    {
                        //Get its distance from lil guy
                        workingVector = position - enemyList[i].Position;
                        workingLength = workingVector.Length();

                        //If the distance is shorter than the one currently in targetEnemy
                        if (workingLength < targetEnemyLength)
                        {
                            //Reassign targetEnemy to the shorter enemy
                            targetEnemy = enemyList[i];
                            targetEnemyLength = workingLength;
                        }
                    }

                    //After loop, set the position to currently move towards as this shortest enemy
                    movingPosition = targetEnemy.Position;
                }

                //Make the lil guy move towards the target at i's defined speed
                movingPosition = movingPosition - position;
                
                
                movingPosition.Normalize();
                movingPosition *= speed;

                //TODO:TEMP:
                if ((game.Player.Position - (position + movingPosition)).Length() > distanceAllowedFrom &&
                    ((game.Player.Position - position).Length() < distanceAllowedFrom))
                    movingPosition = Vector2.Zero;
            }

            if (movingPosition.X < 0 && direction > 1)
                direction--;
            if (movingPosition.X > 0 && direction < 2)
                direction++;
            if (movingPosition.X == 0)
                direction = 0;

            lilGuyAni.Update(gameTime, direction);
            
            //Adjust position
            position += movingPosition;

            //Update collision
            base.Update(new Rectangle((int)position.X, (int)position.Y, 50, 50));

            //Add the badass particle trail
            game.ParticleManager.GenerateParticles(new Vector2(position.X + 20, position.Y + 40), new[] { 2 }, new[] { 15 });

            gun.Fire("BULLET", 1);
            gun.Update(gameTime);
        }

        public new void Draw()
        {
            //Draw a lil guy
            lilGuyAni.Draw(game.SpriteBatch, position);
            //game.SpriteBatch.Draw(lilGuyTexture, position, null, Color.White);

            //Testing - Draw bounding box
            //base.Draw();
        }

                                                #endregion //UPDATE
    }
}
