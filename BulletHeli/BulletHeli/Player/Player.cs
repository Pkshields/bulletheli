﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public class Player : CollisionObject
    {
                                                #region PROPERTIES

        //References
        private TheGame game;                                   //Save reference to the game
        private Texture2D playerTexture;                        //Save player texture
        private Animation animation;                            //Set up animation for the player
        private Gun gun;                                        //Set up gun
        private LilGuy lilGuy;                                  //Set up temp Lil Guy

        //Variables
        private Vector2 position = new Vector2(0, 0);           //Player position
        private Vector2 rectSize = new Vector2(66, 72);         //Viewable player size
        private Vector2 hitboxDimensions = new Vector2(10, 15); //Hit box dimensions
        private Vector2 offset;                                 //Hitbox offset from (0,0)
        private Type targetType;
        private int player;                                     //Player ID (Player 1 or 2) NOT IMPLEMENTED FULLY: PROBLEMS GETTING  2-PLAYER TO WORK, KEPT AS PROOF OF CONCEPT
        private TimeSpan invuln;

        private float health;                                   //Player health
        private float fullHealth;                               //What the original full health was when initialises
        private int lives = 6;                                  //Number of lives the player has remaining TODO:
        private int score = 0;                                  //Score of this particular player
        private bool slow = true;

        //Info
        private int speed = 5;                                  //Speed of the players movement
        private String[] weapon = new String[4];
        private String activeWeapon;
        private int[] weaponAmmo = new int[4];
        private int activeWeaponAmmo;
        private int weaponSelector;
        private Vector2 velocity = Vector2.Zero;
        private string ammoFile;

                                                #endregion //PROPERTIES

                                                #region RETURNS

        //Returns various values needed in other class files

        public Vector2 Velocity
        {
            get { return velocity; }
        }

        public int Width
        {
            get { return (int)rectSize.X; }
        }

        public int Height
        {
            get { return (int)rectSize.Y; }
        }

        public Vector2 Position
        {
            get { return position; }
        }

        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        public float Health
        {
            get { return health; }
            set { health = value; }
        }

        public int Lives
        {
            get { return lives; }
            set { lives = value; }
        }


                                                #endregion //RETURNS

        /*******************************************************************************************************************************/

                                                #region INITIALIZATION

        public void Initialize(TheGame game, int player, float health, Vector2? position)
        {
            //Save TheGame for referencing
            this.game = game;

            //Set player number
            this.player = player;

            //Set inital position of player
            if (position != null) this.position = (Vector2)position;

            //Initalise the animation
            animation = new Animation();
            animation.Initialize(25, 3, 0, rectSize, null, 0, true, false);

            //Initalise the Collision manager
            //Calculate the hit box
            offset = new Vector2((rectSize.X / 2) - (hitboxDimensions.X / 2), (rectSize.Y / 2) - (hitboxDimensions.Y/2) - 17);
            base.Initialize(game, new Rectangle((int)(this.position.X + offset.X), (int)(this.position.Y + offset.Y), (int)hitboxDimensions.X, (int)hitboxDimensions.Y), game.Collision, this.GetType(), offset);
            
            //Properties
            this.health = health;
            fullHealth = health;
            this.targetType = typeof(Enemy);
            this.ammoFile = "Content/Ammo/AmmoTypes.txt";
            this.invuln = TimeSpan.Zero;
            
            //Initalise the gun
            gun = new Gun();
            gun.Initialize(game, this, targetType, ammoFile);

            weapon[0] = "BULLET";
            weapon[1] = "BOMB";
            weapon[2] = "LASER";
            weapon[3] = "MISSILE";

            weaponAmmo[0] = 1;
            weaponAmmo[1] = 1;
            weaponAmmo[2] = 1;
            weaponAmmo[3] = 1;

            weaponSelector = 0;

            activeWeapon = weapon[weaponSelector];
            activeWeaponAmmo = weaponAmmo[weaponSelector];

            //Konami Code fun - Contra 30 lives
            if (game.KonamiCode)
                lives = 30;


            //TEMP
            lilGuy = new LilGuy();
            lilGuy.Initialize(game, 4, 100);

        }

        public void LoadContent()
        {
            //Load the sprite into content manager
            playerTexture = game.ContentManager.Load<Texture2D>("Player/chopper-ani");

            //Send texture to Animation
            animation.LoadContent(playerTexture);
        }

                                                #endregion //INITIALIZATION

                                                #region UPDATE

        
        public void Update(GameTime gameTime)
        {  
            HealthCheck();                                              //Check if the player is still alive
            animation.Update(gameTime);                                 //Apply animation to the player
            gun.Update(gameTime);                                       //Update the gun and it's bullets

            lilGuy.Update(gameTime);                                    //Update the lil guy

            game.ParticleManager.GenerateParticles(new Vector2(position.X + rectSize.X/2, position.Y + rectSize.Y), new[] { 2 }, new[] { 15 });

            gun.Fire(activeWeapon, activeWeaponAmmo);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //Draw lil guy first
            lilGuy.Draw();

            //Draw the player via animation
            animation.Draw(game.SpriteBatch, position);

            //Testing - Draw bounding box
            //base.Draw();
        }

                                                #endregion //UPDATE

        /*******************************************************************************************************************************/

                                                #region MOVEMENT CONTROLS

        /// <summary>
        /// Moves the player around the screen and keeps it within viewing range
        /// Also, for now, handles the bullet generation management
        /// </summary>
        /// <param name="currentKeyboardState"></param>
        /// <param name="previousKeyboardState"></param>
        /// <param name="currentGamePadState"></param>
        /// <param name="previousGamePadState"></param>
        public void Move(int playerID, KeyboardState currentKeyboardState, KeyboardState previousKeyboardState, GamePadState currentGamePadState, GamePadState previousGamePadState)
        {
            int speed;
            if (currentKeyboardState.IsKeyDown(Keys.Space) || currentGamePadState.IsButtonDown(Buttons.A))
                    speed = this.speed - 2;
                else
                    speed = this.speed;
            
            //Reset velocity
            velocity = Vector2.Zero;

            //Get Thumbstick Controls
            velocity.X += currentGamePadState.ThumbSticks.Left.X*speed;
            velocity.Y -= currentGamePadState.ThumbSticks.Left.Y*speed;

            //Check if Thumbsticks are bring used:);
            if (velocity == Vector2.Zero)
            {
                #region PLAYER2 MOVEMENT

                /*************************************************************************
                 * We tried to implement second player controls, it didn't quite work out,
                 * this is here for proof
                 *************************************************************************/
                //// Check all WASD and arrows the move accordingly, setting directional info on the way
                //if ((player == 1 && currentKeyboardState.IsKeyDown(Keys.A)) ||
                //    (player == 2 && currentKeyboardState.IsKeyDown(Keys.Left)) || 
                //    (currentGamePadState.IsButtonDown(Buttons.DPadLeft)))
                //{
                //    velocity.X -= speed;
                //}

                //if ((player == 1 && currentKeyboardState.IsKeyDown(Keys.D)) ||
                //    (player == 2 && currentKeyboardState.IsKeyDown(Keys.Right)) ||
                //    (currentGamePadState.IsButtonDown(Buttons.DPadRight)))
                //{
                //    velocity.X += speed;
                //}

                //if ((player == 1 && currentKeyboardState.IsKeyDown(Keys.W)) ||
                //    (player == 2 && currentKeyboardState.IsKeyDown(Keys.Up)) ||
                //    (currentGamePadState.IsButtonDown(Buttons.DPadUp)))
                //{
                //    velocity.Y -= speed;
                //}

                //if ((player == 1 && currentKeyboardState.IsKeyDown(Keys.S)) ||
                //        (player == 2 && currentKeyboardState.IsKeyDown(Keys.Down)) ||
                //        (currentGamePadState.IsButtonDown(Buttons.DPadDown)))

                //{
                //    velocity.Y += speed;
                //}

                #endregion
                
                // Check all WASD and arrows the move accordingly, setting directional info on the way
                if ((player == 1 && currentKeyboardState.IsKeyDown(Keys.A)) ||
                    (currentGamePadState.IsButtonDown(Buttons.DPadLeft)))
                {
                    velocity.X -= speed;
                }

                if ((player == 1 && currentKeyboardState.IsKeyDown(Keys.D)) ||
                    (currentGamePadState.IsButtonDown(Buttons.DPadRight)))
                {
                    velocity.X += speed;
                }

                if ((player == 1 && currentKeyboardState.IsKeyDown(Keys.W)) ||
                    (currentGamePadState.IsButtonDown(Buttons.DPadUp)))
                {
                    velocity.Y -= speed;
                }

                if ((player == 1 && currentKeyboardState.IsKeyDown(Keys.S)) ||
                    (currentGamePadState.IsButtonDown(Buttons.DPadDown)))
                {
                    velocity.Y += speed;
                }
            }

            //Apply movement values
            position.X += velocity.X;
            position.Y += velocity.Y;

            //Update collision object
            base.Update(position);

            // Make sure that the player does not go out of bounds
            position.X =
                MathHelper.Clamp(base.Rect.X, game.GameDimensions.X, game.GameDimensions.Width - base.Rect.Width) -
                offset.X;
            position.Y =
                MathHelper.Clamp(base.Rect.Y, game.GameDimensions.Y, game.GameDimensions.Height - base.Rect.Height) -
                offset.Y;

            if ((currentKeyboardState.IsKeyDown(Keys.Left) && previousKeyboardState.IsKeyUp(Keys.Left)) || (currentGamePadState.IsButtonDown(Buttons.LeftShoulder) && previousGamePadState.IsButtonUp(Buttons.LeftShoulder)))
            {
                weaponSelector--;
                if (weaponSelector < 0)
                    weaponSelector = 3;

                activeWeapon = weapon[weaponSelector];
                activeWeaponAmmo = weaponAmmo[weaponSelector];
            }

            if ((currentKeyboardState.IsKeyDown(Keys.Right) && previousKeyboardState.IsKeyUp(Keys.Right)) || (currentGamePadState.IsButtonDown(Buttons.RightShoulder) && previousGamePadState.IsButtonUp(Buttons.RightShoulder)))
            {
                weaponSelector++;
                weaponSelector %= 4;

                activeWeapon = weapon[weaponSelector];
                activeWeaponAmmo = weaponAmmo[weaponSelector];
            }

            #region PLAYER 2 SHOOTING

            /***************************************************************************************
             * Like Movement, couldn't make the second player controls work quite right for shooting
             * This is here for proof of concept
             * *************************************************************************************/

            ////First, change gun type
            //if ((player == 1 &&
            //        currentKeyboardState.IsKeyDown(Keys.E) &&
            //        previousKeyboardState.IsKeyUp(Keys.E)) ||
            //    (player == 2 &&
            //        currentKeyboardState.IsKeyDown(Keys.OemPeriod) &&
            //        previousKeyboardState.IsKeyUp(Keys.OemPeriod)) ||
            //    (currentGamePadState.IsButtonDown(Buttons.LeftShoulder) &&
            //     previousGamePadState.IsButtonUp(Buttons.LeftShoulder)))
            //{
            //    currentGun++;

            //    if (currentGun > gun.NumGuns)
            //        currentGun = 1;
            //}

            ////Then, fire bullets!
            //if ((player == 1 &&
            //        currentKeyboardState.IsKeyDown(Keys.Space)) ||
            //    (player == 2 &&
            //        currentKeyboardState.IsKeyDown(Keys.NumPad0)) ||
            //    (currentGamePadState.IsButtonDown(Buttons.A)))
            //{
            //    gun.Bullet(currentGun);
            //} 

            #endregion
        }

                                                #endregion

                                                #region COLLISION MANAGEMENT

        //Apply collision if required
        public override void OnCollision(CollisionObject obj)
        {
            Type objType = obj.Type;

            //If collision is with an enemy
            if (obj.Type == targetType)
            {
                //Take the damage that the enemy applied away from the player health
                Enemy objItem = (Enemy)obj;
                health -= objItem.Damage;
            }
            
            //If collision is with a bullet
            else if (objType.BaseType == typeof(Attack))
            {
                //Take the damage that the bullet applied away from the enemy health
                Attack objItem = (Attack)obj;
                if (objItem.Target == this.GetType())
                {
                    if (objType == typeof(Laser))
                    {
                        if (invuln <= TimeSpan.Zero)
                        {
                            health -= objItem.Damage;
                            invuln = objItem.Invulnerable;
                        }
                    }
                    else
                        health -= objItem.Damage;
                }
            }
        }

        public void HealthCheck()
        {
            //If the player is out of lives
            if (health <= 0)
                //If the player still has lives, remove one and keep going
                if (lives > 0)
                {
                    lives--;
                    health = fullHealth;
                }
                //Else die sucka
                else
                {
                    OnDeath();
                }

            //If the player has TOO much life!
            if (health > fullHealth)
            {
                //Make sure health doesnt go over max
                health = fullHealth;
            }
        }

                                                #endregion //COLLISION MANAGEMENT

        public void Upgrade()
        {
            int x = -1;
            
            switch (weaponSelector)
            {
                case 0:
                    x = gun.BulletCount - 1;
                    break;
                case 1:
                    x =  gun.BombCount - 1;
                    break;
                case 2:
                    x = gun.LaserCount - 1;
                    break;
                case 3:
                    x = gun.MissileCount - 1;
                    break;
            }

            if (weaponAmmo[weaponSelector] < x)
            {
                weaponAmmo[weaponSelector]++;
                activeWeaponAmmo = weaponAmmo[weaponSelector];
            }

        }

                                                #region GAME STATE MANAGEMENT

        //Adapted from PauseMenuScreenScreen
        public void OnDeath()
        {
            //Send score to server
            LeaderboardData.Set(game.Username, score.ToString());

            //Game over screen of sorts
            const string message = "Game Over\n\n Your score has been submitted to the leaderboard";
            MessageBoxScreen confirmQuitMessageBox = new MessageBoxScreen(message, false);
            confirmQuitMessageBox.Accepted += ReturnToMenu;
            game.ScreenManager.AddScreen(confirmQuitMessageBox, game.ControllingPlayer);

            //Fade out music
            game.Audio.Stop(game.MusicCue);
        }

        private void ReturnToMenu(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(game.ScreenManager, true, null, new BackgroundScreen(),
                                                           new MainMenuScreen());
        }
                                                #endregion //GAME STATE MANAGEMENT
    }
}