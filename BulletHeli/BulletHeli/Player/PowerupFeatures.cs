﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class PowerupFeatures
    {
        /// <summary>
        /// When a powerup is active/collected, it comes to this method and activates the 
        /// powerups features. This keeps the Powerup and PowerupObject classes nice and clean, 
        /// leaving them without as much specific code as possible. Like the text files we are using 
        /// for gun/enemies, but with more features 
        /// </summary>
        /// <param name="game">Reference to the main game</param>
        /// <param name="type">type of powerup to activate</param>
        public static void RunFeature(TheGame game, int type)
        {
            switch (type)
            {
                case 1:
                    //Add 20 health points to the player
                    game.Player.Health += 20;
                    break;
                case 2:
                    //Add 50 points to the players total
                    game.Player.Score += 50;
                    break;
                case 3:
                    game.Player.Upgrade();
                    break;
                case 4:
                    //Add 1 life to playa
                    game.Player.Lives += 1;
                    break;
            }
        }
    }
}
