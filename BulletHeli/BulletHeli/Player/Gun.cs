﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class Gun
    {
                                                #region PROPERTIES

        //Bullet List
        private List<Bullet> bulletList = new List<Bullet>();
        private List<Bomb> bombList = new List<Bomb>();
        private List<Laser> laserList = new List<Laser>();

        List<Array> bulletType;

        //References
        private TheGame game;           //Game reference
        private Player player;          //Player Reference

        //Settings
        private Texture2D bulletTexture;//Bullet Texture

        //Time
        private TimeSpan timeBetween;   //Time between bullets being shot
        private TimeSpan lastTime;      //Last time a bullet was shot

                                                #endregion //PROPERTIES

                                                #region RETURNS

        //Return width and height
        public int NumGuns
        {
            get { return bulletType.Count; }
        }

                                                #endregion //RETURNS


                                                #region INITIALIZE

        public void Initialize(TheGame game, Player player)
        {
            //Save game reference
            this.game = game;
            this.player = player;

            //TIME
            this.timeBetween = TimeSpan.FromSeconds(1f);
            lastTime = TimeSpan.Zero;

            //Get list of bullet types from the gun types text file
            bulletType = parseTXT("Content/Ammo/AmmoTypes.txt", 3);
        }

        public void LoadContent()
        {
            //Load the sprite
            bulletTexture = game.ContentManager.Load<Texture2D>("Ammo/PlayerBullet");
        }

                                                #endregion //INITIALIZE

                                                #region UPDATE
        /// <summary>
        /// Updates every bullet in the bullet list (whatever they need to update) 
        /// and check if they are still active (remove them from list)
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            int bulletCount = bulletList.Count;
            int bombCount = bombList.Count;
            int laserCount = laserList.Count;

            for (int i = 0; i < bulletCount; i++)
            {
                //Update bullet
                bulletList[i].Update(gameTime);

                //check if it is still active
                if (!bulletList[i].Active)
                {
                    bulletList.Remove(bulletList[i]);
                    i--;
                    bulletCount--;
                }
            }

            for (int i = 0; i < bombCount; i++)
            {
                //Update bullet
                bombList[i].Update(gameTime);

                //check if it is still active
                if (!bombList[i].Active)
                {
                    bombList.Remove(bombList[i]);
                    i--;
                    bombCount--;
                }
            }

            for (int i = 0; i < laserCount; i++)
            {
                Vector2 newPosition = player.Position;
                newPosition.X += player.Width/2 - bulletTexture.Width/2;
                newPosition.Y += bulletTexture.Height;

                //Update bullet
                laserList[i].Update(gameTime, newPosition);

                //check if it is still active
                if (!laserList[i].Active)
                {
                    laserList.Remove(laserList[i]);
                    i--;
                    laserCount--;
                }
            }

            if(lastTime > TimeSpan.Zero)
                lastTime -= gameTime.ElapsedGameTime;
        }

        /// <summary>
        /// Draw every bullet left on the list
        /// </summary>
        public void Draw()
        {
            foreach (var bullet in bulletList)
            {
                bullet.Draw();
            }

            foreach (var bomb in bombList)
            {
                bomb.Draw();
            }

            foreach (var laser in laserList)
            {
                laser.Draw();
            }
        }

                                            #endregion //UPDATE

        /*******************************************************************************************************************************/

                                                #region FIRE BULLET

        /// <summary>
        /// Fires the gun. Depending on the gun type, more than one bullet can be shot at different speeds and at different angles
        /// to the player. Delay between bullet shots are dependant on the gun type
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="type">Gun type</param>
        public void Fire(GameTime gameTime, int type)
        {
            //Get the timeBetween for the weapon we are currently on
            type--;
            timeBetween = TimeSpan.FromSeconds(Convert.ToDouble(bulletType[type].GetValue(3)));

            //If we are due shooting a new bullet)
            if (lastTime <= TimeSpan.Zero)
            {
                //Find out what bullet type it is and see how manny bullets it should spawn
                int bulletNum = Convert.ToInt32(bulletType[type].GetValue(0));
                int bulletAngle = Convert.ToInt32(bulletType[type].GetValue(1));
                int bulletSpeed = Convert.ToInt32(bulletType[type].GetValue(2));
                double temp = Convert.ToDouble(bulletType[type].GetValue(4));
                float bulletDmg = (float)temp;

                //Generate required number of bullets
                for (int i = 0; i < bulletNum; i++)
                {
                    //Calculate the offset based on the number of bullets and angle
                    float angle = (float)bulletNum/2;
                    angle -= (float)(i + 0.5);
                    angle = angle*bulletAngle;

                    //Generate a new bullet at player position and add it to the bullet list
                    //Calculate position based on player (start at centre of player)
                    Vector2 newPosition = player.Position;
                    newPosition.X += player.Width / 2 - bulletTexture.Width / 2;
                    newPosition.Y += player.Height / 2 - bulletTexture.Height / 2;

                    Bullet newBullet = new Bullet();
                    newBullet.Initialize(game, bulletTexture, newPosition, bulletSpeed, angle, bulletDmg);
                    bulletList.Add(newBullet);
                }

                //Update last bullet shot time
                lastTime = timeBetween;
            }
        }

        public void Fire(Vector2 position, int type)
        {
            //Get the timeBetween for the weapon we are currently on
            type--;
            timeBetween = TimeSpan.FromSeconds(Convert.ToDouble(bulletType[type].GetValue(3)));

            //Find out what bullet type it is and see how manny bullets it should spawn
            int bulletNum = Convert.ToInt32(bulletType[type].GetValue(0));
            int bulletAngle = Convert.ToInt32(bulletType[type].GetValue(1));
            int bulletSpeed = Convert.ToInt32(bulletType[type].GetValue(2));
            double temp = Convert.ToDouble(bulletType[type].GetValue(4));
            float bulletDmg = (float)temp;
            
            //Generate required number of bullets
            for (int i = 0; i < bulletNum; i++)
            {
                //Calculate the offset based on the number of bullets and angle
                float angle = (float)bulletNum / 2;
                angle -= (float)(i + 0.5);
                angle = angle * bulletAngle;

                Bullet newBullet = new Bullet();
                newBullet.Initialize(game, bulletTexture, position, bulletSpeed, angle, bulletDmg);
                bulletList.Add(newBullet);
            }
        }

        public void FireBomb(GameTime gameTime)
        {
            //Get the timeBetween for the weapon we are currently on
            timeBetween = TimeSpan.FromSeconds(0.5);

            //If we are due shooting a new bullet)
            if (lastTime <= TimeSpan.Zero)
            {
                //Find out what bullet type it is and see how manny bullets it should spawn
                int bulletNum = 1;
                int bulletAngle = 0;
                int bulletSpeed = 10;
                //double temp = Convert.ToDouble(bulletType[type].GetValue(4));
                float bulletDmg = 75.0f;

                //Generate required number of bullets
                for (int i = 0; i < bulletNum; i++)
                {
                    //Calculate the offset based on the number of bullets and angle
                    float angle = (float)bulletNum / 2;
                    angle -= (float)(i + 0.5);
                    angle = angle * bulletAngle;

                    //Generate a new bullet at player position and add it to the bullet list
                    //Calculate position based on player (start at centre of player)
                    Vector2 newPosition = player.Position;
                    newPosition.X += player.Width / 2 - bulletTexture.Width / 2;
                    newPosition.Y += bulletTexture.Height / 2;

                    Bomb newBullet = new Bomb();
                    newBullet.Initialize(game, this, bulletTexture, newPosition, bulletSpeed, angle, timeBetween, bulletDmg);
                    bombList.Add(newBullet);
                }

                //Update last bullet shot time
                lastTime = timeBetween;
            }
        }

        public void FireLaser(GameTime gameTime)
        {
            //Get the timeBetween for the weapon we are currently on
            timeBetween = TimeSpan.FromSeconds(2);

            //If we are due shooting a new bullet)
            if (lastTime <= TimeSpan.Zero)
            {
                //double temp = Convert.ToDouble(bulletType[type].GetValue(4));
                TimeSpan duration = new TimeSpan();
                duration = TimeSpan.FromSeconds(1);
                float bulletDmg = 75.0f;

                //Generate a new bullet at player position and add it to the bullet list
                //Calculate position based on player (start at centre of player)
                Vector2 newPosition = player.Position;
                newPosition.X += player.Width / 2 - bulletTexture.Width / 2;
                newPosition.Y += bulletTexture.Height / 2;

                Laser newBullet = new Laser();
                newBullet.Initialize(game, bulletTexture, newPosition, duration, bulletDmg);
                laserList.Add(newBullet);

                //Update last bullet shot time
                lastTime = timeBetween;
            }
        }

                                                #endregion //FIRE BULLET

                                                #region PARSETXT

        /// <summary>
        /// Takes in a file name then parses it out into a list containing arrays, each array containing all the words from
        /// a line of text from the file. Option to start reading from a certain line added in to allow for formatting in the
        /// text file itself
        /// </summary>
        /// <param name="file">File we are parsing</param>
        /// <param name="startLine">Line to start parsing from</param>
        /// <returns></returns>
        private List<Array> parseTXT (string file, int startLine)
        {
            //List we are generating
            List<Array> newList = new List<Array>();

            //Open file for parsing
            using (StreamReader sr = new StreamReader(file))
            {
                String line;
                string[] words;

                //Manually move the fine pointer to the start of the lines to parse
                for (int i = 1; i < startLine; i++)
                {
                    line = sr.ReadLine();
                }

                //Parse every line into an array, using a space as the delimiter
                while ((line = sr.ReadLine()) != null)
                {
                    words = line.Split(' ');
                    newList.Add(words);
                }
            }
            
            //Return list to gun
            return newList;
        }

                                                #endregion //PARSETXT
    }
}

