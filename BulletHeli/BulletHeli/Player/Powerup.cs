﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public class Powerup
    {
                                                #region PROPERTIES

        //References
        private TheGame game;                                               //Save reference to the game
        private List<Texture2D> textureList = new List<Texture2D>();        //Store the possible powerup textures
        private List<PowerupObject> powerupList = new List<PowerupObject>();//List of all currently active powerups

                                                #endregion //PROPERTIES

                                                #region RETURNS



                                                #endregion //RETURNS

        /*******************************************************************************************************************************/

                                                #region INITIALIZATION

       public void Initialize(TheGame game)
        {
            //Save TheGame for referencing
            this.game = game;
        }

        public void LoadContent()
        {
            //Set up all textures
            TextureSetup();
        }

                                                #endregion //INITIALIZATION

                                                #region UPDATE

        public void Update()
        {
            //Update all currently active powerups
            int totalPowerups = powerupList.Count;
            for (int i = 0; i < totalPowerups; i++)
            {
                powerupList[i].Update();

                //If powerup is no longer active, remove it from game and collision
                if (!powerupList[i].Active)
                {
                    powerupList[i].RemoveCollision();
                    powerupList.Remove(powerupList[i]);
                    i--;
                    totalPowerups--;
                }
            }
        }

        public void Draw()
        {
            //Draw all currently active powerups
            foreach (var powerup in powerupList)
                powerup.Draw();
        }

                                                #endregion //UPDATE

        /*******************************************************************************************************************************/

                                                #region POWERUP METHODS

        public void TextureSetup()
        {
            //Add all possible powerup sprites to the list
            textureList.Add(game.ContentManager.Load<Texture2D>("Powerups/UndeadHealth"));
            textureList.Add(game.ContentManager.Load<Texture2D>("Powerups/UndeadNuke"));
            textureList.Add(game.ContentManager.Load<Texture2D>("Powerups/weaponUp"));
            textureList.Add(game.ContentManager.Load<Texture2D>("UI/truck"));
        }

        /// <summary>
        /// When this is called, the powerup manager determines if a powerup should be spawned
        /// If so, generates it and adds it to currently active powerups list
        /// </summary>
        /// <param name="position">Position to generate powerup at if necessary</param>
        public void AddPowerup(Vector2 position)
        {
            //Generate a number between 1 and 50
            Random random = new Random();
            int ranNum = random.Next(0, 60);
            int? powerupNum = null;

            //Detect which powerup should run
            if (ranNum >= 0 && ranNum < 5) 
                powerupNum = 0;
            else if (ranNum >= 11 && ranNum <= 20) 
                powerupNum = 1;
            else if (ranNum >= 21 && ranNum <= 25) 
                powerupNum = 2;
            else if (ranNum == 50) 
                powerupNum = 3;

            //Detect if a powerup should be generated and if so, generate it
            if (powerupNum != null)
            {
                PowerupObject obj = new PowerupObject();
                obj.Initialize(game, textureList[(int)powerupNum], position, (int)powerupNum+1);
                powerupList.Add(obj);
            }
        }

                                                #endregion //POWERUP METHODS
    }
}
