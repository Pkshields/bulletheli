﻿#region USING
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion //USING

namespace BulletHeli
{
    class Gun
    {
        #region PROPERTIES
        List<Array> bulletType;
        List<Array> bombType;
        List<Array> laserType;
        List<Array> missileType;

        //References
        private TheGame game;                   //Game reference
        private CollisionObject obj;            //CollisionObject Reference
        private Type targetType;                //Type of enemy the firer targets
        
        //Settings
        private Texture2D attackTexture;        //Texture used by the various attacks

        //Time
        private TimeSpan timeBetween;           //Time between attacks being shot
        private TimeSpan lastTime;              //Last time an attack was shot

        #endregion //PROPERTIES

        #region RETURNS

        public int BulletCount
        {
            get { return bulletType.Count; }
        }

        public int BombCount
        {
            get { return bombType.Count; }
        }

        public int LaserCount
        {
            get { return laserType.Count; }
        }

        public int MissileCount
        {
            get { return missileType.Count; }
        }
        #endregion //RETURNS

        #region INITIALIZE

        /// <summary>
        /// 
        /// </summary>
        /// <param name="game">Game reference</param>
        /// <param name="obj">Firer Reference</param>
        /// <param name="targetType">Target Reference</param>
        /// <param name="ammoFile">The file the Firer uses to create it's ammo lists</param>
        public void Initialize(TheGame game, CollisionObject obj, Type targetType, string ammoFile)
        {            
            //Save the game references
            this.game = game;
            this.obj = (CollisionObject)obj;
            this.targetType = targetType;

            //Time management for firing rate
            this.lastTime = TimeSpan.Zero;
            
            //Get list of attack types from the 'ammoFile' text file
            bulletType = parseTXT(ammoFile, "BULLETS");
            bombType = parseTXT(ammoFile, "BOMBS");
            laserType = parseTXT(ammoFile, "LASERS");
            missileType = parseTXT(ammoFile, "MISSILES");

        }

        #endregion //INITIALIZE

        #region UPDATE
        /// <summary>
        /// Used to manage the firing rate of the gun
        /// </summary>
        /// <param name="gameTime">Needed to update the time between attacks</param>
        public void Update(GameTime gameTime)
        {
            if (lastTime > TimeSpan.Zero)
            {
                lastTime -= gameTime.ElapsedGameTime;
            }
        }

        #endregion //UPDATE

        /*******************************************************************************************************************************/

        #region FIRE BULLET

        /// <summary>
        /// Fires the gun. Depending on the attack type, more than one of the same kind of attack
        /// can be shot at by the firer. Delay between bullet shots are dependant on the attack
        /// </summary>
        /// <param name="weapon">Determines which method is called to handle the desired attack</param>
        /// <param name="ammo">ammo type for the particular attack/weapon</param>
        public void Fire(string weapon, int ammo = -1)
        {
            ammo--;

            if (weapon.Equals("BULLET"))
            {
                Bullet(ammo);
            }
            if (weapon.Equals("BOMB"))
            {
                Bomb(ammo);
            }
            if (weapon.Equals("LASER"))
            {
                Laser(ammo);
            }
            if (weapon.Equals("MISSILE"))
            {
                if (game.EnemyManager.EnemyList.Count > 0)
                    Missile(ammo);
            }
        }
        
        public void Bullet(int ammo)
        {
             //If we are due shooting a new attack
            if (lastTime <= TimeSpan.Zero)
            {
                //Assigns the texture to be used by the attack
                attackTexture = game.ContentManager.Load<Texture2D>("Ammo/" + bulletType[ammo].GetValue(5));

                //Creates the rate of fire
                timeBetween = TimeSpan.FromSeconds(Convert.ToDouble(bulletType[ammo].GetValue(3)));

                //Find out what bullet type it is and see how manny bullets it should spawn
                int bulletNum = Convert.ToInt32(bulletType[ammo].GetValue(0));
                float bulletAngle = (float)Convert.ToDouble(bulletType[ammo].GetValue(1));
                int bulletSpeed = Convert.ToInt32(bulletType[ammo].GetValue(2));
                float bulletDmg = (float)Convert.ToDouble(bulletType[ammo].GetValue(4));
                float offset = (float)Convert.ToDouble(bulletType[ammo].GetValue(6));

                //Generate required number of bullets)
                for (int i = 0; i < bulletNum; i++)
                {
                    //Calculate the offset based on the number of bullets and angle
                    float angle = (float)bulletNum / 2;
                    angle -= (float)(i + 0.5);
                    angle = angle * bulletAngle + offset;

                    //Calculate position based on CollisionObject (start at centre of CollisionObject)
                    Vector2 newPosition = obj.ObjPosition;
                    newPosition.X += obj.ObjWidth / 2 - attackTexture.Width / 2;

                    //Adjusts the position based on the targetType, 'Enemy' will shift it completely to thebottom of its Rectangle
                    if (obj.Type != typeof(Player))
                        newPosition.Y += obj.ObjHeight + 1;
                    else
                        newPosition.Y -= attackTexture.Height;

                    //Creates the list of variables used to spawn the bullet and then calls the Attack Manager to create the bullet
                    object[] list = new object[] { game, targetType, attackTexture, newPosition, bulletSpeed, angle, bulletDmg };
                    game.Attack.Add(list, typeof(Bullet));
                }

                //Update last bullet shot time
                lastTime = timeBetween;
            }
        }

        public void Shrapnel(int ammo, float offset)
        {
            ammo--;

            //Assigns the texture to be used
            if (targetType != typeof(Enemy))
                attackTexture = game.ContentManager.Load<Texture2D>("Ammo/enemy" + bulletType[ammo].GetValue(5));
            else
                attackTexture = game.ContentManager.Load<Texture2D>("Ammo/player" + bulletType[ammo].GetValue(5));

            //Find out what bullet type it is and see how manny bullets it should spawn
            int bulletNum = Convert.ToInt32(bulletType[ammo].GetValue(0));
            float bulletAngle = (float)Convert.ToDouble(bulletType[ammo].GetValue(1));
            int bulletSpeed = Convert.ToInt32(bulletType[ammo].GetValue(2));
            float bulletDmg = (float)Convert.ToDouble(bulletType[ammo].GetValue(4));

            //Generate required number of bullets
            for (int i = 0; i < bulletNum; i++)
            {
                //Calculate the offset based on the number of bullets and angle, as well as the angle of the bomb
                float angle = (float)bulletNum / 2 + offset / ((float)bulletNum / 2);
                angle -= (float)(i + 0.5);
                angle = angle * bulletAngle;


                //Calculate position based on bomb (start at centre of bomb)
                Vector2 newPosition = obj.ObjPosition;
                newPosition.X += obj.ObjWidth / 2 - attackTexture.Width / 2;
                newPosition.Y += obj.ObjHeight / 2 - attackTexture.Height / 2;

                //Creates the list of variables used to spawn the bullet and then calls the Attack Manager to create the bullet
                object[] list = new object[] { game, targetType, attackTexture, newPosition, bulletSpeed, angle, bulletDmg };
                game.Attack.Add(list, typeof(Bullet));
            }
        }

        private void Bomb(int ammo)
        {
            //If we are due shooting a new attack
            if (lastTime <= TimeSpan.Zero)
            {
                //Creates the animation to be used by the bomb
                attackTexture = game.ContentManager.Load<Texture2D>("Ammo/" + bombType[ammo].GetValue(7));
                Animation animation = new Animation();
                int frameTime = Convert.ToInt32(bombType[ammo].GetValue(8));
                int frames = Convert.ToInt32(bombType[ammo].GetValue(9));
                int start = Convert.ToInt32(bombType[ammo].GetValue(10));
                int X = Convert.ToInt32(bombType[ammo].GetValue(11));
                int Y = Convert.ToInt32(bombType[ammo].GetValue(12));
                bool animate = Convert.ToBoolean(bombType[ammo].GetValue(13));
                animation.Initialize(frameTime, frames, 0, new Vector2(X, Y), null, 0, animate);
                animation.LoadContent(attackTexture);
                float offset = (float)Convert.ToDouble(bombType[ammo].GetValue(14));

                //Creates the rate of fire
                timeBetween = TimeSpan.FromSeconds(Convert.ToDouble(bombType[ammo].GetValue(4)));

                //Find out what bomb type it is and see how many bombs it should spawn
                int bombNum = Convert.ToInt32(bombType[ammo].GetValue(0));
                float bombAngle = (float)Convert.ToDouble(bombType[ammo].GetValue(1));
                int bombSpeed = Convert.ToInt32(bombType[ammo].GetValue(2));
                TimeSpan fuse = TimeSpan.FromSeconds(Convert.ToDouble(bombType[ammo].GetValue(4)));
                float bombDmg = (float)Convert.ToDouble(bombType[ammo].GetValue(5));
                int shrapnel = Convert.ToInt32(bombType[ammo].GetValue(6));

                //Generate required number of bombs
                for (int i = 0; i < bombNum; i++)
                {
                    //Calculate the offset based on the number of bombs and angle
                    float angle = (float)bombNum / 2;
                    angle -= (float)(i + 0.5);
                    angle = angle * bombAngle + offset;


                    //Calculate position based on CollisionObject (start at centre of CollisionObject)
                    Vector2 newPosition = obj.ObjPosition;
                    newPosition.X += obj.ObjWidth / 2 - animation.Size.Width / 2;

                    if (obj.Type != typeof(Player))
                        newPosition.Y += obj.ObjHeight + 1;
                    else
                        newPosition.Y -= animation.Size.Width;

                    object[] list = new object[] { game, targetType, animation, newPosition, bombSpeed, angle, fuse, bombDmg, shrapnel };
                    game.Attack.Add(list, typeof(Bomb));
                }

                //Update last bombs shot time
                lastTime = timeBetween;
            }
        }

        private void Laser(int ammo)
        {
            //If we are due shooting a new laser
            if (lastTime <= TimeSpan.Zero)
            {
                //Get the timeBetween for the attack we are currently on
                TimeSpan laserDuration = TimeSpan.FromSeconds(Convert.ToDouble(laserType[ammo].GetValue(0)));
                TimeSpan laserInvuln = TimeSpan.FromSeconds(Convert.ToDouble(laserType[ammo].GetValue(2)));

                timeBetween = TimeSpan.FromSeconds(Convert.ToDouble(laserType[ammo].GetValue(1)));
                timeBetween += laserDuration;

                attackTexture = game.ContentManager.Load<Texture2D>("Ammo/" + laserType[ammo].GetValue(5));

                float laserDmg = (float)Convert.ToDouble(laserType[ammo].GetValue(4));
                int laserWidth = Convert.ToInt32(laserType[ammo].GetValue(3));


                //Calculate position based on CollisionObject (start at centre of CollisionObject)
                Vector2 newPosition = obj.ObjPosition;
                newPosition.X += obj.ObjWidth / 2 - laserWidth / 2;

                if (targetType != typeof(Enemy))
                    newPosition.Y += obj.ObjHeight / 2;
                else
                    newPosition.Y += obj.ObjHeight + 3;

                object[] list = new object[] { game, targetType, attackTexture, obj, newPosition, laserDuration, laserInvuln, laserDmg, laserWidth };
                game.Attack.Add(list, typeof(Laser));

                //Update last laser shot time
                lastTime = timeBetween;
            }
        }

        private void Missile(int ammo)
        {
            //If we are due shooting a new attack
            if (lastTime <= TimeSpan.Zero)
            {
                //Get the timeBetween for the attack we are currently on
                TimeSpan delay = TimeSpan.FromSeconds(Convert.ToDouble(missileType[ammo].GetValue(5)));
                timeBetween = TimeSpan.FromSeconds(Convert.ToDouble(missileType[ammo].GetValue(6)));

                attackTexture = game.ContentManager.Load<Texture2D>("Ammo/" + missileType[ammo].GetValue(7));

                float maxAccel = (float)Convert.ToDouble(missileType[ammo].GetValue(0));
                float maxSpeed = (float)Convert.ToDouble(missileType[ammo].GetValue(1));
                float slowRadius = (float)Convert.ToDouble(missileType[ammo].GetValue(2));
                float arriveRadius = (float)Convert.ToDouble(missileType[ammo].GetValue(3));
                float missileDmg = (float)Convert.ToDouble(missileType[ammo].GetValue(4));

                //Calculate position based on CollisionObject (start at centre of CollisionObject)
                Vector2 newPosition = obj.ObjPosition;
                newPosition.X += obj.ObjWidth / 2;

                if (targetType != typeof(Enemy))
                    newPosition.Y += obj.ObjHeight + attackTexture.Height / 2;
                else
                    newPosition.Y -= attackTexture.Height / 2;

                object[] list = new object[] { game, targetType, attackTexture, newPosition, maxAccel, maxSpeed, slowRadius, arriveRadius, missileDmg, delay };
                game.Attack.Add(list, typeof(Missile));
                lastTime = timeBetween;
            }
        }

        #endregion //FIRE BULLET

        #region PARSETXT

        /// <summary>
        /// Takes in a file name then parses it out into a list containing arrays of Strings, each array containing all the words from
        /// a line of text from the file. Option to start reading from a certain line added in to allow for formatting in the
        /// text file itself
        /// </summary>
        /// <param name="file">File we are parsing</param>
        /// <param name="type">key word which start parsing from</param>
        /// <returns></returns>
        private static List<Array> parseTXT(string file, string type)
        {
            //List we are generating
            List<Array> newList = null;

            //Open file for parsing
            using (StreamReader sr = new StreamReader(file))
            {
                String line = sr.ReadLine();
                string[] words;

                //Manually move the fine pointer to the start of the lines to parse
                while (!type.Equals(line))
                {
                    line = sr.ReadLine();

                    if (sr.EndOfStream)
                        return newList;
                }

                newList = new List<Array>();

                //Parse every line into an array, using a space as the delimiter
                while (!(line = sr.ReadLine()).Equals("END"))
                {
                    words = line.Split(' ');
                    newList.Add(words);
                }
            }

            //Return list to gun
            return newList;
        }

        #endregion //PARSETXT
    }
}

