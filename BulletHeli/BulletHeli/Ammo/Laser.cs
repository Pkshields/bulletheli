﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class Laser : Attack
    {
        #region PROPERTIES

        //References
        private TheGame game;                               //Game reference
        private Texture2D attackTexture;                    //BulletTexture reference
        private CollisionObject obj;

        //Settings
        private Vector2 position = new Vector2();           //Bullet Position
        private bool active;                                //If the laser is active
        private TimeSpan duration;
        private TimeSpan invuln;
        private Rectangle collBox;
        private Type targetType;
        private float dmg;
        private int width;

        #endregion //PROPERTIES

        #region RETURNS

        //Return Content Manager
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public CollisionObject Firer
        {
            get { return obj; }
        }

        public int Width
        {
            get { return width; }
        }

        public override float Damage
        {
            get { return dmg; }
        }

        public override Type Target
        {
            get { return targetType; }
        }

        #endregion //RETURNS

        #region INITIALIZE

        public void Initialize(TheGame game, Type targetType, Texture2D attackTexture, CollisionObject obj, Vector2 position, TimeSpan duration, TimeSpan invuln, float dmg, int width)
        {
            //Save game reference
            this.game = game;
            this.attackTexture = attackTexture;
            this.obj = (CollisionObject)obj;

            //Settings
            this.position.X = position.X;
            this.duration = duration;
            this.invuln = invuln;
            this.active = true;
            this.dmg = dmg;
            this.width = width;
            this.collBox.Width = this.width;
            this.collBox.X = (int)this.position.X;
            this.targetType = targetType;

            if (this.targetType != typeof(Enemy))
            {
                this.position.Y = game.GameDimensions.Height;
                this.collBox.Y = (int)position.Y;
                this.collBox.Height = (int)(this.position.Y - position.Y);
            }
            else
            {
                this.position.Y = 0;
                this.collBox.Y = (int)this.position.Y;
                this.collBox.Height = (int)position.Y;
            }

            //Initalise the Collision manager
            base.Initialize(game, collBox, game.Collision, this.GetType(), null);
        }

        #endregion //INITIALIZE

        #region UPDATE

        /// <summary>
        /// Updates the laser position, updates it's rect with the collision manager and
        /// checks if it has left rthe screen, deactivating it
        /// TODO: Add collision handling to it
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime, Vector2 position)
        {
            this.position.X = position.X;
            collBox.X = (int)this.position.X;

            if (this.targetType != typeof(Enemy))
            {
                collBox.Y = (int) position.Y;
                this.collBox.Height = (int)(this.position.Y - position.Y);
            }
            else
                this.collBox.Height = (int)position.Y;

            //Update collision object
            base.Update(collBox);

            if (duration <= TimeSpan.Zero)
            {
                //Deactivate
                active = false;
                base.RemoveCollision();
            }

            duration -= gameTime.ElapsedGameTime;
        }

        public new void Draw()
        {
            //Draw laser!
            game.SpriteBatch.Draw(attackTexture, collBox, attackTexture.Bounds, Color.White);
        }

        #endregion //UPDATE

    }
}
