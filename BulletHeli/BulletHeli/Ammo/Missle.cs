﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class Missile : CollisionObject
    {
        #region PROPERTIES

        //References
        private TheGame game;                               //Game reference
        private Texture2D attackTexture;                    //BulletTexture reference
        private Player player;

        //Settings
        private Vector2 position = new Vector2(400, 50);       //Bullet Position
        private float dmg;
        private bool active;
        private bool rocket;
        private bool lockedOn;
        private float rotation;
        private Vector2 origin;
        private float time;
        private Vector2 velocity;
        private float maxAccel;
        private float maxSpeed;
        private float arriveRadius;
        private float slowRadius;
        private float targetSpeed;
        private TimeSpan delay;

        #endregion //PROPERTIES

        #region RETURNS

        //Return Content Manager
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public float Damage
        {
            get { return dmg; }
        }

        #endregion //RETURNS

        #region INITIALIZE

        public void Initialize(TheGame game, float maxAccel, float maxSpeed, float arriveRadius, float slowRadius)
        {
            //Save game reference
            this.game = game;
            this.player = game.Player;
            this.attackTexture = game.ContentManager.Load<Texture2D>("Enemy/Arrow");

            //Settings
            //this.position = position;
            this.active = true;
            this.rocket = false;
            this.lockedOn = false;
            this.dmg = 50;
            this.origin = new Vector2(attackTexture.Width/2, attackTexture.Height/2);
            this.maxAccel = maxAccel;
            this.maxSpeed = maxSpeed;
            this.arriveRadius = arriveRadius;
            this.slowRadius = slowRadius;
            time = 0;
            delay = TimeSpan.FromSeconds(0.2);

            Rectangle hitbox = attackTexture.Bounds;
            hitbox.Width /= (int) 10;
            hitbox.Height /= (int) 10;

            rotation = (float)Math.Atan2(0, -1);

            //Initalise the Collision manager)
            base.Initialize(game, hitbox, game.Collision, this.GetType(), null);
        }

        #endregion //INITIALIZE

        #region UPDATE

        /// <summary>
        /// Updates the bullet position, updates it's rect with the collision manager and
        /// checks if it has left rthe screen, deactivating it
        /// TODO: Add collision handling to it
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            time += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (!rocket)
            {
                velocity = Arrive();
                rotation = DetermineOrientation();
            }
            else
            {
                if (delay <= TimeSpan.Zero && !lockedOn)
                {
                    velocity = player.ObjPosition - position;
                    velocity.Normalize();
                    velocity *= maxSpeed;
                    lockedOn = true;
                }
                else if (delay > TimeSpan.Zero)
                {
                    delay -= gameTime.ElapsedGameTime;
                    velocity = player.ObjPosition - position;
                    velocity.Normalize();
                    rotation = DetermineOrientation();
                    velocity = Vector2.Zero;
                }
            }

            position += velocity;

            //Update collision object
            base.Update(position);

            //Check if the bullet has left the boundaries of the screen
            if (position.X < game.GameDimensions.X ||
                position.Y < game.GameDimensions.Y ||
                position.X > game.GameDimensions.Width ||
                position.Y > game.GameDimensions.Height)
            {
                //Deactivate
                active = false;
                base.RemoveCollision();
            }
        }

       // private int i = 0;
        public void Draw(SpriteBatch spriteBatch)
        {
            //if (i%2 == 1)
            //Draw enemy!
            game.SpriteBatch.Draw(attackTexture, position, null, Color.White, rotation, origin, 0.1f, SpriteEffects.None, 0);

            //i++;
            //base.Draw();
        }

        #endregion //UPDATE

        #region COLLISION MANAGEMENT

        //Apply collision if required
        public override void OnCollision(CollisionObject obj)
        {
            //If collision is with an enemy
            if (obj.Type.Name.Equals("Enemy"))
            {
                //Remove the bullet from the screenm and collision
                base.RemoveCollision();
                active = false;
            }
        }
        #endregion //COLLISION MANAGEMENT

        public Vector2 Arrive()
        {
            float slowingFactor = 5;

	        Vector2 acceleration = new Vector2(0,0);
	        Vector2 direction = player.ObjPosition - position;
	        float distance = direction.Length();

	        if( distance < arriveRadius )
	        {
	            rocket = true;
                return acceleration;
	        }

            if (distance > slowRadius)
                if(targetSpeed < maxSpeed)
                    targetSpeed += maxAccel;
                else
                    targetSpeed = maxSpeed;
            else
                targetSpeed = (maxSpeed * distance) / slowRadius;

            direction.Normalize();
            Vector2 targetVelocity = direction;
                
            targetVelocity *= targetSpeed;

            acceleration = targetVelocity - velocity;
	        acceleration /= slowingFactor;

            if (acceleration.Length() > maxAccel)
            {
                acceleration.Normalize();
                acceleration *= maxAccel;
            }

            return acceleration;
        }

        public float DetermineOrientation()
        {
        	if( velocity.Length() == 0 )
		        return rotation;
            else
        	    return (float)Math.Atan2(velocity.X, -velocity.Y);
        }
    }
}
