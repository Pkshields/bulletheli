﻿using System;

namespace BulletHeli
{
    /// <summary>
    /// Attack Class extends CollisionObject Class. This is to allow access
    /// to the Classes that extend Attack by other CollisionObject items
    /// </summary>
    class Attack : CollisionObject
    {
        #region RETURNS
        //Virtual method for the child class to override. Must be overridden or else an infinite loop happens.
        //Allows CollisionObject items to access the damage variable in child classes. This variable is obviously 
        //used to inflict damage to the CollisionObject item
        public virtual float Damage
        {
            get { return Damage; }
        }

        //Virtual method for the child class to override. Must be overridden or else an infinite loop happens.
        //Allows CollisionObject items to aquire the targetType variable in child classes. This variable is used to 
        //Decide whether collision applies to the CollisionObject, as well as the child class or not
        public virtual Type Target
        {
            get { return Target; }
        }

        public virtual TimeSpan Invulnerable
        {
            get { return TimeSpan.Zero; }
        }
        #endregion
    }
}
