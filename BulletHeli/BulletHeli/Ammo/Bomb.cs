﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    /// <summary>
    /// More advanced attack. Can be used by anything with a gun but is only used by Player and Enemy classes.Similar to Bullet.
    /// Travels until it leaves the screen, collides with a particular object or the fuse expires. Upon the fuse expiring or 
    /// the bomb Colliding the bomb spawns a number of bullets in different directions.
    /// Extends the Attack Class which in turn extends the CollisionObject class
    /// </summary>
    class Bomb : Attack
    {
        #region PROPERTIES

        //References
        private TheGame game;                               //Game reference
        private Animation animation;                        //Bomb animation reference
        private Gun gun;                                    //Gun used to shoot the shrapnel 
        private Type targetType;                            //Type reference for collision, bullet will only collide with objects with the same type as this

        //Settings
        private Vector2 position = new Vector2(0, 0);       //Bomb Position
        private TimeSpan fuse;                              //Fuse is a count to the bomb 'exploding'
        private bool active;                                //Bool used for the bomb
        private double moveX;                               //Amount to move in the X axis
        private double moveY;                               //Amount to move in the Y axis
        private float dmg;                                  //Amount of damage the bullet does
        private float angle;                                //Angle used to adjust the angles of the bullets
        private int shrapnel;                               //Int used to select the shrapnel to use
        private string ammoFile;                            //String used to reference the Shrapnel text file

        #endregion //PROPERTIES

        #region RETURNS

        //Return whether the bomb is active or not
        public bool Active
        {
            get { return active; }
        }

        // Override return method used for to pass the damage along to a CollisionObject which has collided with the bullet
        public override float Damage
        {
            get { return dmg; }
        }

        // Override return method used for to pass the targetType along to a CollisionObject which has collided with the bullet
        public override Type Target
        {
            get { return targetType; }
        }

        #endregion //RETURNS

        #region INITIALIZE

        /// <summary>
        /// Used to set the properties of a Bomb object
        /// </summary>
        /// <param name="game">Game reference</param>
        /// <param name="targetType">Type of object the bomb collides with</param>
        /// <param name="animation">The sprite for the bomb</param>
        /// <param name="position">The position the bomb starts from</param>
        /// <param name="speed">The speed which the bomb travels a</param>
        /// <param name="angle">The angle of the bomb. Used to determine the overall direction of the bullet</param>
        /// <param name="fuse">Used to decide when the bomb 'explodes'</param>
        /// <param name="dmg">How much damage the bombdoes when it collides with an objecy matching the targetType</param>
        /// <param name="shrapnel">Declares the shrapnel type the bomb uses</param>
        public void Initialize(TheGame game, Type targetType, Animation animation, Vector2 position, int speed, float angle, TimeSpan fuse, float dmg, int shrapnel)
        {
            //Save game reference
            this.game = game;
            this.animation = animation;
            this.targetType = targetType;

            //Settings
            this.position = position;                       //Sets the position of the bomb to the vector passed in
            this.active = true;                             //Ensures that the bullet is active
            this.fuse = fuse;                               //Time until the bomb expoldes
            this.dmg = dmg;                                 //Sets the damage to the value passed in
            this.gun = new Gun();                           //Creates a new Gun class for Bomb to use to 'explode'
            this.shrapnel = shrapnel;                       //Shrapnel to used when the fuse expires
            this.ammoFile = "Content/Ammo/Shrapnel.txt";
            this.angle = angle;                             //Angle used later by shrapnel to ensure the expolsion is correct

            //Calculate the amount the bomb has to move in each direction based on the angle
            double radians = this.angle * (Math.PI / 180);

            //Reverses the speed if the targetType is not equal to Type "Enemy"
            //i.e. The bullet travels in the opposite direction if fired by an object of Type "Enemy"
            if (this.targetType != typeof(Enemy))
                speed *= -1;

            moveY = speed * Math.Cos(radians);  //Calculates the amount to move along the Y-axis per Update call
            moveX = speed * Math.Sin(radians);  //Calculates the amount to move along the X-axis per Update call

            //Initalize the Collision manager
            base.Initialize(game, animation.Size, game.Collision, this.GetType(), null);
        }

        #endregion //INITIALIZE

        #region UPDATE

        /// <summary>
        /// Updates the bomb position, updates it's Rectangle with the Collision Manager and
        /// checks if it has left the screen, deactivating it if it has. It also checks to see if
        /// fuse has expired, and if it has it explodes, or if not, reduces the amount of time left
        /// int fuse
        /// </summary>
        /// <param name="gameTime">Used to determine the amount of time elapsed</param>
        public void Update(GameTime gameTime)
        {
            //Move the bomb around the screen
            position.Y -= (float)moveY;
            position.X += (float)moveX;

            //Update collision object
            base.Update(position);

            //Updates the animation
            animation.Update(gameTime);

            //Check if the bomb has left the boundaries of the screen
            if (position.X < game.GameDimensions.X ||
                position.Y < game.GameDimensions.Y ||
                position.X > game.GameDimensions.Width ||
                position.Y > game.GameDimensions.Height)
            {
                active = false;             //Deactivate
                base.RemoveCollision();     //Removes the bomb from the collision list
            }

            if (fuse <= TimeSpan.Zero)
            {
                gun.Initialize(game, this, targetType, ammoFile);   //Initialises the gun to shoot the shrapnel apon explosion
                gun.Shrapnel(shrapnel, angle);                      //Causes the shrapnel, which is actually bullets, to spawn.
                active = false;                                     //Deactivate
                base.RemoveCollision();                             //Removes the bomb from the collision list
            }

            //Decreases the amount of time left until the fuse expires
            fuse -= gameTime.ElapsedGameTime;
        }

        /// <summary>
        /// Draws the bomb animation on screen.
        /// </summary>
        public new void Draw()
        {
            //Calls animation to 'draw' the bombs, specific animation, using SpriteBatch and the current position
            animation.Draw(game.SpriteBatch, position);
        }

        #endregion //UPDATE

        #region COLLISION MANAGEMENT

        /// <summary>
        /// Override method used for collision. Checks to see if the bomb has collided with
        /// a CollisionObject, and then checks to see if the Type of CollisionObject is the same as the targetType.
        /// If so, it intializes the gun, fires shrapnel, removes the bomb from the collision list and then deactivates it
        /// </summary>
        /// <param name="obj">The CollisionObject the bullet has potentially collided with</param>
        public override void OnCollision(CollisionObject obj)
        {
            //If collision is with an enemy
            if (obj.Type == targetType)
            {
                gun.Initialize(game, this, targetType, ammoFile);
                active = false;
                base.RemoveCollision();
                gun.Shrapnel(shrapnel, angle);
            }
        }

        #endregion //COLLISION MANAGEMENT
    }
}
