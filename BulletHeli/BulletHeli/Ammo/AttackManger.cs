﻿#region USING
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion //USING

namespace BulletHeli
{
    /// <summary>
    /// AttackManager class used to add, update, draw and remove the wide variety of attacks. A manager is
    /// needed because the firer of the attack may deactive before the attack does. This would leave the attack
    /// in place but not draw or update it. The manager prevents this.
    /// </summary>
    public class AttackManger
    {
        #region LISTS AND QUEUES
        //Attack Lists and queues
        private List<Bullet> bulletList = new List<Bullet>();           //List of bullets currently active
        private Queue<Bullet> freeBulletList = new Queue<Bullet>();     //Queue of deactivated, hence free, bullets
        private List<Bomb> bombList = new List<Bomb>();                 //List of bombs currently active
        private Queue<Bomb> freeBombList = new Queue<Bomb>();           //Queue of deactivated, hence free, bombs
        private List<Laser> laserList = new List<Laser>();              //List of lasers currently active
        private Queue<Laser> freeLaserList = new Queue<Laser>();        //Queue of deactivated, hence free, lasers
        private List<Missile> missileList = new List<Missile>();        //List of missiles currently active
        private Queue<Missile> freeMissileList = new Queue<Missile>();  //Queue of deactivated, hence free, missiles 
        #endregion

        #region ADD
        /// <summary>
        /// Add method called by the various Gun Classes to add 'new' attack to the game
        /// </summary>
        /// <param name="list">An array of variables of the called attack. Used to initialize it</param>
        /// <param name="attack">Type of attack to add. Used to determine which specific method manager should use</param>
        public void Add(object[] list, Type attack)
        {
            //If Type "Attack" is a match to the Type "Bullet", calls the Bullet method and passes the array "list" to said method, then leaves this method, Add, entirely
            if (attack == typeof(Bullet))
            {
                Bullet(list);
                return;
            }
            //If Type "Attack" is a match to the Type "Bomb", calls the Bomb method and passes the array "list" to said method, then leaves this method, Add, entirely
            if (attack == typeof(Bomb))
            {
                Bomb(list);
                return;
            }
            //If Type "Attack" is a match to the Type "Laser", calls the Laser method and passes the array "list" to said method, then leaves this method, Add, entirely
            if (attack == typeof(Laser))
            {
                Laser(list);
                return;
            }
            //If Type "Attack" is a match to the Type "Missile", calls the Missile method and passes the array "list" to said method, then leaves this method, Add, entirely
            if (attack == typeof(Missile))
            {
                Missile(list);
                return;
            }
        } 
        #endregion
        
        #region UPDATE
        /// <summary>
        /// Updates every attack in the attack list, checks to see if they're still active,
        /// and if not, adds them to their respective queue, then removes them from their respective list
        /// </summary>
        /// <param name="gameTime">Needed for some of the attacks to update</param>
        public void Update(GameTime gameTime)
        {
            int bulletCount = bulletList.Count;     //Current number of bullets in bulletList, used for the For Loop
            int bombCount = bombList.Count;         //Current number of bombs in bombList, used for the For Loop
            int laserCount = laserList.Count;       //Current number of lasers in laserList, used for the For Loop
            int missileCount = missileList.Count;   //Current number of missiles in missileList, used for the For Loop

            for (var i = 0; i < laserCount; i++)
            {
                //Calculates the new postion for the current laser
                Vector2 newPosition = laserList[i].Firer.ObjPosition;
                newPosition.X += laserList[i].Firer.ObjWidth / 2 - laserList[i].Width / 2;

                //Adjusts the new position depending on the targetType of the laser
                if (laserList[i].Target != typeof(Enemy))
                    newPosition.Y += (float)laserList[i].Firer.ObjHeight / 2;
                else
                    newPosition.Y += laserList[i].Firer.ObjHeight + 3;

                //Updates the current laser
                laserList[i].Update(gameTime, newPosition);

                //checks to see if it is still active
                if (!laserList[i].Active)
                {
                    freeLaserList.Enqueue(laserList[i]);    //Adds to queue if it is not
                    laserList.Remove(laserList[i]);         //Removes from list if it is not
                    i--;                                    //Sets the loop counter back 1
                    laserCount--;                           //Lowers the condition needed to be met to exit the For Loop
                }
            }

            for (int i = 0; i < missileCount; i++)
            {
                //Updates the current missile
                missileList[i].Update(gameTime);

                //checks to see if it is still active
                if (!missileList[i].Active)
                {
                    freeMissileList.Enqueue(missileList[i]);    //Adds to queue if it is not
                    missileList.Remove(missileList[i]);         //Removes from list if it is not
                    i--;                                        //Sets the loop counter back 1
                    missileCount--;                             //Lowers the condition needed to be met to exit the For Loop
                }

            }

            for (var i = 0; i < bombCount; i++)
            {
                //Updates the current bomb
                bombList[i].Update(gameTime);

                //checks to see if it is still active
                if (!bombList[i].Active)
                {
                    freeBombList.Enqueue(bombList[i]);      //Adds to queue if it is not
                    bombList.Remove(bombList[i]);           //Removes from list if it is not
                    i--;                                    //Sets the loop counter back 1
                    bombCount--;                            //Lowers the condition needed to be met to exit the For Loop

                }
            }
            
            //Goes through each bullet in the bulletList
            for (var i = 0; i < bulletCount; i++)
            {
                //Updates the current bullet
                bulletList[i].Update();

                //checks to see if it is still active
                if (!bulletList[i].Active)
                {
                    freeBulletList.Enqueue(bulletList[i]);  //Adds to queue if it is not
                    bulletList.Remove(bulletList[i]);       //Removes from list if it is not
                    i--;                                    //Sets the loop counter back 1
                    bulletCount--;                          //Lowers the condition needed to be met to exit the For Loop
                }
            }
        }

        /// <summary>
        /// Draws every attack left on the list
        /// </summary>
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var bullet in bulletList)
            {
                bullet.Draw();
            }

            foreach (var bomb in bombList)
            {
                bomb.Draw();
            }

            foreach (var laser in laserList)
            {
                laser.Draw();
            }

            foreach (var missile in missileList)
            {
                missile.Draw(spriteBatch);
            }
        }

        #endregion //UPDATE

        #region ATTACK INITIALISATION
        /// <summary>
        /// Checks the relevant queue, if it is occupied it re-initializes the attack, then adds it back to the relelvant attack list
        /// </summary>
        /// <param name="list">An array of variables of the called attack. Used to initialize it</param>
        private void Bullet(object[] list)
        {
            if (freeBulletList.Count > 0)
            {
                Bullet temp = freeBulletList.Dequeue();
                temp.Initialize((TheGame)list[0], (Type)list[1], (Texture2D)list[2], (Vector2)list[3], (int)list[4], (float)list[5], (float)list[6]);
                bulletList.Add(temp);
            }
            else
            {
                Bullet newBullet = new Bullet();
                newBullet.Initialize((TheGame)list[0], (Type)list[1], (Texture2D)list[2], (Vector2)list[3], (int)list[4], (float)list[5], (float)list[6]);
                bulletList.Add(newBullet);
            }
        }

        /// <summary>
        /// Checks the relevant queue, if it is occupied it re-initializes the attack, then adds it back to the relelvant attack list
        /// </summary>
        /// <param name="list">An array of variables of the called attack. Used to initialize it</param>
        private void Bomb(object[] list)
        {
            if (freeBombList.Count > 0)
            {
                Bomb temp = freeBombList.Dequeue();
                temp.Initialize((TheGame)list[0], (Type)list[1], (Animation)list[2], (Vector2)list[3], (int)list[4], (float)list[5], (TimeSpan)list[6], (float)list[7], (int)list[8]);
                bombList.Add(temp);
            }
            else
            {
                Bomb newBomb = new Bomb();
                newBomb.Initialize((TheGame)list[0], (Type)list[1], (Animation)list[2], (Vector2)list[3], (int)list[4], (float)list[5], (TimeSpan)list[6], (float)list[7], (int)list[8]);
                bombList.Add(newBomb);
            }
        }

        /// <summary>
        /// Checks the relevant queue, if it is occupied it re-initializes the attack, then adds it back to the relelvant attack list
        /// </summary>
        /// <param name="list">An array of variables of the called attack. Used to initialize it</param>
        private void Laser(object[] list)
        {
            if (freeLaserList.Count > 0)
            {
                Laser temp = freeLaserList.Dequeue();
                temp.Initialize((TheGame)list[0], (Type)list[1], (Texture2D)list[2], (CollisionObject)list[3], (Vector2)list[4], (TimeSpan)list[5], (TimeSpan)list[6], (float)list[7], (int)list[8]);
                laserList.Add(temp);
            }
            else
            {
                Laser newLaser = new Laser();
                newLaser.Initialize((TheGame)list[0], (Type)list[1], (Texture2D)list[2], (CollisionObject)list[3], (Vector2)list[4], (TimeSpan)list[5], (TimeSpan)list[6], (float)list[7], (int)list[8]);
                laserList.Add(newLaser);
            }
        }

        /// <summary>
        /// Checks the relevant queue, if it is occupied it re-initializes the attack, then adds it back to the relelvant attack list
        /// </summary>
        /// <param name="list">An array of variables of the called attack. Used to initialize it</param>
        private void Missile(object[] list)
        {
            if (freeMissileList.Count > 0)
            {
                Missile temp = freeMissileList.Dequeue();
                temp.Initialize((TheGame)list[0], (Type)list[1], (Texture2D)list[2], (Vector2)list[3], (float)list[4], (float)list[5], (float)list[6], (float)list[7], (float)list[8], (TimeSpan)list[9]);
                missileList.Add(temp);
            }
            else
            {
                Missile newMissle = new Missile();
                newMissle.Initialize((TheGame)list[0], (Type)list[1], (Texture2D)list[2], (Vector2)list[3], (float)list[4], (float)list[5], (float)list[6], (float)list[7], (float)list[8], (TimeSpan)list[9]);
                missileList.Add(newMissle);
            }
        }
    }
}

        #endregion