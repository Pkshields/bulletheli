﻿#region USING
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion //USING

namespace BulletHeli
{
    /// <summary>
    /// Basic attack used by anything with a gun. Travels until it leaves the screen or collides with a particular object.
    /// Extends the Attack Class which in turn extends the CollisionObject class
    /// </summary>
    class Bullet : Attack
    {
        #region PROPERTIES

        //References
        private TheGame game;                               //Game reference
        private Texture2D attackTexture;                    //BulletTexture reference
        private Type targetType;                            //Type reference for collision, bullet will only collide with objects with the same type as this

        //Settings
        private Vector2 position = new Vector2(0, 0);       //Bullet Position
        private bool active;                                //If the bullet is active
        private Vector2 velocity =  new Vector2(0,0);       //bullet velocity
        private double y;
        private double moveX;                               //Amount to move in the X axis
        private double moveY;                               //Amount to move in the Y axis
        private float dmg;                                  //Amount of damage the bullet does
        //private Vector2 velocity;

        #endregion //PROPERTIES

        #region RETURNS

        //Return whether the bullet is active or not
        public bool Active
        {
            get { return active; }
        }

        // Override return method used for to pass the damage along to a CollisionObject which has collided with the bullet
        public override float Damage
        {
            get { return dmg; }
        }

        // Override return method used for to pass the targetType along to a CollisionObject which has collided with the bullet
        public override Type Target
        {
            get { return targetType; }
        }

        #endregion //RETURNS

        #region INITIALIZE

        /// <summary>
        /// Used set the properties of a Bullet object.
        /// </summary>
        /// <param name="game">Game reference</param>
        /// <param name="targetType">Type of object the bullet collides with</param>
        /// <param name="attackTexture">The sprite for the bullet</param>
        /// <param name="position">The position the bullet starts from</param>
        /// <param name="speed">The speed which the bullet travels at</param>
        /// <param name="angle">The angle of the bullet. Used to determine the overall direction of the bullet</param>
        /// <param name="dmg">How much damage the bullet does when it collides with an objecy matching the targetType</param>
        public void Initialize(TheGame game, Type targetType, Texture2D attackTexture, Vector2 position, int speed, float angle, float dmg)
        {
            //Save references
            this.game = game;                       
            this.attackTexture = attackTexture;     
            this.targetType = targetType;           

            //Settings
            this.position = position;       //Sets the position of the bullet to the vector passed in
            this.active = true;             //Ensures that the bullet is active
            this.dmg = dmg;                 //Sets the damage to the value passed in
            this.y = 0;

            //Calculate the amount the bullet has to move in each direction based on the angle
            double radians = angle * (Math.PI / 180);

            //Reverses the speed if the targetType is not equal to Type "Enemy"
            //i.e. The bullet travels in the opposite direction if fired by an object of Type "Enemy"
            if (targetType != typeof(Enemy))
            speed *= -1;//Currently inverts speed!
            

            moveY = speed * Math.Cos(radians);  //Calculates the amount to move along the Y-axis per Update call
            moveX = speed * Math.Sin(radians);  //Calculates the amount to move along the X-axis per Update call
            //if (targetType != typeof(Player))
            //moveX += game.Player.Velocity.X;//TEST: Maintains bullet momentum during player movement

            //Initalize the Collision manager
            base.Initialize(game, attackTexture.Bounds, game.Collision, this.GetType(), null);
        }

        #endregion //INITIALIZE

        #region UPDATE

        /// <summary>
        /// Updates the bullet position, updates it's Rectangle with the Collision Manager and
        /// checks if it has left the screen, deactivating it if it has.
        /// </summary>
        public void Update()
        {
            //Move the bullet around the screen
            position.Y -= (float) moveY;
            position.X += (float) moveX;


            //Update CollisionObject
            base.Update(position);

            //Check if the bullet has left the boundaries of the screen
            if (position.X < game.GameDimensions.X - (attackTexture.Width / 2) ||
                position.Y < game.GameDimensions.Y - (attackTexture.Height / 2) ||
                position.X > game.GameDimensions.Width - (attackTexture.Width / 2) ||
                position.Y > game.GameDimensions.Height - (attackTexture.Height / 2))
            {
                active = false;             //Deactivate
                base.RemoveCollision();     //Removes the bullet from the collision list
            }
        }

        private float SinWave()
        {
            moveX = (Math.Cos(y / 50)*2);
            y += 1;
            return (float) moveX;
        }

        /// <summary>
        /// Draws the bullet on screen.
        /// </summary>
        public new void Draw()
        {
            //Calls SpriteBatch from "game" and uses it to draw the bullet
            game.SpriteBatch.Draw(attackTexture, position, attackTexture.Bounds, Color.White);
        }

        #endregion //UPDATE

        #region COLLISION MANAGEMENT

        /// <summary>
        /// Override method used for collision. Checks to see if the bullet has collided with
        /// a CollisionObject, and then checks to see if the Type of CollisionObject is the same as the targetType.
        /// If so, it removes the bullet from the collision list and deactivates it
        /// </summary>
        /// <param name="obj">The CollisionObject the bullet has potentially collided with</param>
        public override void OnCollision(CollisionObject obj)
        {
            //If collision is with the targetType
            if (obj.Type == targetType)
            {
                //Remove the bullet from the screen and the collision list if the Types match
                base.RemoveCollision();
                active = false;
            }
        }

        #endregion //COLLISION MANAGEMENT
    }
}
