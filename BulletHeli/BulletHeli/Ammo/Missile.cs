﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class Missile : Attack
    {
        #region PROPERTIES

        //References
        private TheGame game;                               //Game reference
        private Texture2D attackTexture;                    //BulletTexture reference
        private CollisionObject obj;

        //Settings
        private Vector2 position;                           //Bullet Position
        private Vector2 origin;
        private Vector2 velocity;
        private TimeSpan delay;
        private Type targetType;
        private bool rocket;
        private bool lockedOn;
        private bool active;
        private float dmg;
        private float time;
        private float rotation;
        private float maxAccel;
        private float maxSpeed;
        private float arriveRadius;
        private float slowRadius;
        private float targetSpeed;
        //private float health;

        #endregion //PROPERTIES

        #region RETURNS

        //Return Content Manager
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public override float Damage
        {
            get
            {
                return dmg;
            }
        }

        public override Type Target
        {
            get
            {
                return obj.Type;
            }
        }

        #endregion //RETURNS

        #region INITIALIZE

        public void Initialize(TheGame game, Type targetType, Texture2D attackTexture, Vector2 position, float maxAccel, float maxSpeed, float arriveRadius, float slowRadius, float dmg, TimeSpan delay)
        {
            //Save game reference
            this.game = game;
            this.attackTexture = attackTexture;

            //Settings
            this.position = position;
            this.active = true;
            this.rocket = false;
            this.lockedOn = false;
            this.dmg = dmg;
            this.origin = new Vector2(attackTexture.Width / 2, attackTexture.Height / 2);
            this.targetType = targetType;
            this.maxAccel = maxAccel;
            this.maxSpeed = maxSpeed;
            this.arriveRadius = arriveRadius;
            this.slowRadius = slowRadius;
            this.time = 0;
            this.delay = delay;

            Rectangle hitbox = attackTexture.Bounds;

            if (this.targetType != typeof(Enemy))
                obj = (CollisionObject)game.Player;
            else
            {
                List<Enemy> enemyList = game.EnemyManager.EnemyList;
                int enemy = 0;
                Vector2 distance = enemyList[enemy].ObjPosition - position;
                for (int i = 1; i < enemyList.Count; i++)
                {
                    Vector2 temp = enemyList[i].ObjPosition - position;
                    if (temp.Length() < distance.Length())
                    {
                        distance = temp;
                        enemy = i;
                    }
                }

                obj = (CollisionObject)enemyList[enemy];
            }

            Vector2 offset = new Vector2(((this.attackTexture.Width / 2) - attackTexture.Width), ((this.attackTexture.Height / 2) - attackTexture.Height));

            //health = 60f;

            rotation = (float)Math.Atan2(0, -1);

            //Initalise the Collision manager)
            base.Initialize(game, hitbox, game.Collision, this.GetType(), offset);
        }

        #endregion //INITIALIZE

        #region UPDATE

        /// <summary>
        /// Updates the missile position, updates it's rect with the collision manager and
        /// checks if it has left rthe screen, deactivating it
        /// TODO: Add collision handling to it
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            time += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (!rocket)
            {
                velocity = Arrive();
                rotation = DetermineOrientation();
            }
            else
            {
                if (delay <= TimeSpan.Zero && !lockedOn)
                {
                    velocity = obj.ObjPosition - position;
                    velocity.Normalize();
                    velocity *= maxSpeed;
                    lockedOn = true;
                }
                else if (delay > TimeSpan.Zero)
                {
                    delay -= gameTime.ElapsedGameTime;
                    velocity = obj.ObjPosition - position;
                    velocity.Normalize();
                    rotation = DetermineOrientation();
                    velocity = Vector2.Zero;
                }
            }

            position += velocity;

            //Update collision object
            base.Update(position);

            //Check if the missile has left the boundaries of the screen
            if (position.X < game.GameDimensions.X ||
                position.Y < game.GameDimensions.Y ||
                position.X > game.GameDimensions.Width ||
                position.Y > game.GameDimensions.Height)
            {
                //Deactivate
                active = false;
                base.RemoveCollision();
            }

            //if (health <= 0)
            //{
            //    //Deactivate
            //    active = false;
            //    base.RemoveCollision();
            //}
        }

        // private int i = 0;
        public void Draw(SpriteBatch spriteBatch)
        {
            //if (i%2 == 1)
            //Draw enemy!
            game.SpriteBatch.Draw(attackTexture, position, null, Color.White, rotation, origin, 1, SpriteEffects.None, 0);

            //i++;
            //base.Draw();
        }

        #endregion //UPDATE

        #region COLLISION MANAGEMENT

        //Apply collision if required
        public override void OnCollision(CollisionObject obj)
        {
            Type objType = obj.Type;

            //If collision is with an player
            if (objType == targetType)
            {
                //Remove the missile from the screen and collision
                base.RemoveCollision();
                active = false;
            }
            //else if (objType.BaseType == typeof(Attack))
            //{
            //    Attack objItem = (Attack)obj;
            //    if (objItem.Target != typeof(Player))
            //        health -= objItem.Damage;
            //}
        }
        #endregion //COLLISION MANAGEMENT

        public Vector2 Arrive()
        {
            float slowingFactor = 5;

            Vector2 acceleration = new Vector2(0, 0);
            Vector2 direction = obj.ObjPosition - position;
            float distance = direction.Length();

            if (distance < arriveRadius)
            {
                rocket = true;
                return acceleration;
            }

            if (distance > slowRadius)
                if (targetSpeed < maxSpeed)
                    targetSpeed += maxAccel;
                else
                    targetSpeed = maxSpeed;
            else
                targetSpeed = (maxSpeed * distance) / slowRadius;

            direction.Normalize();
            Vector2 targetVelocity = direction;

            targetVelocity *= targetSpeed;

            acceleration = targetVelocity - velocity;
            acceleration /= slowingFactor;

            if (acceleration.Length() > maxAccel)
            {
                acceleration.Normalize();
                acceleration *= maxAccel;
            }

            return acceleration;
        }

        public float DetermineOrientation()
        {
            if (velocity.Length() == 0)
                return rotation;
            else
                return (float)Math.Atan2(velocity.X, -velocity.Y);
        }
    }
}
