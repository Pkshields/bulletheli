﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion //USING

namespace BulletHeli
{
    public class Controls : GameScreen
    {
        #region PROPERTIES

        //Managers and such
        private ContentManager content;
        private SpriteBatch spriteBatch;
        private GraphicsDevice graphicsDevice;

        //Properties
        private float pauseAlpha;                   //Used for sweet fade in
        private Vector2 topCenter;                  //Top center of the screen, used to draw stuff
        private SpriteFont headerFont;              //header font
        private SpriteFont font;                    //regular game font
        private string musicCue = "GameMusic";      //Name of the music cue we are playing on this screen


        //Text
        String header;
        Vector2 headerPosition;
        Vector2[] keysposition = new Vector2[10];
        Vector2[] descrPosition = new Vector2[10];
        Vector2[] controllerPosition = new Vector2[10];
        String[] keys = new String[10];
        String[] descr = new String[10];
        String[] controller = new String[10];


        #endregion //PROPERTIES

        #region RETURNS

        #endregion //RETURNS

        public Controls()
        {
            //GameScreenManager setup
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        private void Initialize()
        {
            //Save system references
            graphicsDevice = ScreenManager.GraphicsDevice;

            //Set up the top center of the screen
            topCenter = new Vector2(ScreenManager.GraphicsDevice.Viewport.Width / 2, 0);

            //Load font
            headerFont = content.Load<SpriteFont>("Fonts/GameFont");
            font = content.Load<SpriteFont>("UI/TheFont");

            //Header?!
            header = "Controls";
            headerPosition = headerFont.MeasureString(this.header);
            headerPosition = new Vector2(topCenter.X - (headerPosition.X / 2), 20);
            float columnPosition = 100;

            keys[0]  = "W A S D, Arrow Keys";
            controller[0] = "Left Thumbstick";
            descr[0] = "Movement";
            keys[1] = "Spacebar";
            controller[1] = "A";
            descr[1] = "Fires primary weapons";
            keys[2] = "C";
            controller[2] = "Y";
            descr[2] = "Fires Bomb";
            keys[3] = "V";
            controller[3] = "X";
            descr[3] = "Fires Laser";
            keys[4]  = "B";
            controller[4] = "B";
            descr[4] = "Fires Missile";
            keys[5] = "1";
            controller[5] = "LB";
            descr[5] = "Next Bullet type";
            keys[6] = "2";
            controller[6] = "RB";
            descr[6] = "Next Bomb type";
            keys[7] = "3";
            controller[7] = "RT";
            descr[7] = "Next Laser type";
            keys[8] = "4";
            controller[8] = "LT";
            descr[8] = "Next Missile type";
            
            

            for (int i = 0; i < 9; i++)
            {
                keysposition[i] = new Vector2(topCenter.X - 400, columnPosition);
                controllerPosition[i] = new Vector2(topCenter.X - 100, columnPosition);
                descrPosition[i] = new Vector2(topCenter.X + 200, columnPosition);
                columnPosition += 40;


            }

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            //Initialize the game
            Initialize();

            //Play the donuts song!
            ScreenManager.Audio.Play(musicCue);
        }

        public override void UnloadContent()
        {
            content.Unload();
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            //Update game screen
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();

            //Draw header
            spriteBatch.DrawString(headerFont, header, headerPosition, Color.White);

            //Draw all names
            for (int i = 0; i < 9; i++)
            {
                spriteBatch.DrawString(font, keys[i], keysposition[i], Color.White);
                spriteBatch.DrawString(font, controller[i], controllerPosition[i], Color.White);
                spriteBatch.DrawString(font, descr[i], descrPosition[i], Color.White);

                spriteBatch.DrawString(font, "Press Escape or start to exit", new Vector2(500,650), Color.White);
            }

            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        /*******************************************************************************************************************************/

        public override void HandleInput(InputState input)
        {
            //Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            //Gets input states to run from
            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            if (keyboardState.IsKeyDown(Keys.Escape) ||
                keyboardState.IsKeyDown(Keys.Enter) ||
                gamePadState.IsButtonDown(Buttons.A) ||
                gamePadState.IsButtonDown(Buttons.Start))
            {
                //Fade out music
                ScreenManager.Audio.Stop(musicCue);
                LoadingScreen.Load(ScreenManager, true, null, new BackgroundScreen(),
                                                           new MainMenuScreen());
            }
        }
    }
}