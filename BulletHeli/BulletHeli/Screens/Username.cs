﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion //USING

namespace BulletHeli
{
    public class Username : GameScreen
    {
        #region PROPERTIES

        //Managers and such
        private ContentManager content;
        private SpriteBatch spriteBatch;
        private GraphicsDevice graphicsDevice;

        //Properties
        private float pauseAlpha;                   //Used for sweet fade in
        private Vector2 topCenter;                  //Top center of the screen, used to draw stuff
        private SpriteFont headerFont;              //Huge bold font for header
        private SpriteFont font;                    //Normal game font for the actual leaderboard
        private string musicCue = "LeaderboardMusic";//Name of the music cue we are playing on this screen
        private bool konamiCode;

        //Text!
        private String header;                      //Header of the screen
        private Vector2 headerPosition;             //Position of the header
        private String username = "Jimson";         //Username string, defaulted to Jimson, is editable by user
        private Vector2 usernamePosition;           //Position of above string

        private int levelNum;

        #endregion //PROPERTIES

        #region RETURNS

        #endregion //RETURNS

        /// <summary>
        /// Creates a new username screen
        /// </summary>
        /// <param name="konamiCode">Bool stating if the user has entered the Konami Code</param>
        public Username(int levelNum, bool konamiCode)
        {
            //GameScreenManager setup
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            //Store the konami code to pass onto the game
            this.levelNum = levelNum;
            this.konamiCode = konamiCode;
        }

        /// <summary>
        /// Initializes the username screen
        /// </summary>
        private void Initialize()
        {
            //Save system references
            graphicsDevice = ScreenManager.GraphicsDevice;

            //Set up the top center of the screen
            topCenter = new Vector2(ScreenManager.GraphicsDevice.Viewport.Width / 2, 0);

            //Load font
            headerFont = content.Load<SpriteFont>("Fonts/GameFont");
            font = content.Load<SpriteFont>("UI/TheFont");

            //Header?!
            header = "Enter Username";
            headerPosition = headerFont.MeasureString(this.header);
            headerPosition = new Vector2(topCenter.X - (headerPosition.X / 2), 20);
        }

        /// <summary>
        /// Loads any content required within the leaderboard screen
        /// 
        /// Also initializes the leaderboard since GSM doesn't support Initialize
        /// Plays donuts song. Why not?!
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            //Initialize the game
            Initialize();

            //Play the donuts song!
            ScreenManager.Audio.Play(musicCue);
        }

        /// <summary>
        /// Unloads all username content when it is no longer needed
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }

        /// <summary>
        /// Update everything required in username screen
        /// </summary>
        /// <param name="gameTime">Current Game Time TimeSpan values</param>
        /// <param name="otherScreenHasFocus">Detects if this screen has user focus</param>
        /// <param name="coveredByOtherScreen">Detects if this screen is covered by another screen</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            //Update game screen
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            //Update username info
            usernamePosition = headerFont.MeasureString(this.username);
            usernamePosition = new Vector2(topCenter.X - (usernamePosition.X / 3.5f), 100);
        }

        /// <summary>
        /// Draw everything in username screen
        /// </summary>
        /// <param name="gameTime">Current Game Time TimeSpan values</param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();

            //Draw
            spriteBatch.DrawString(headerFont, header, headerPosition, Color.White);
            spriteBatch.DrawString(font, username, usernamePosition, Color.White);

            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        /*******************************************************************************************************************************/

        /// <summary>
        /// Uses GSM method to handle input controls
        /// 
        /// Basic text code from: http://www.gamedev.net/topic/457783-xna-getting-text-from-keyboard/
        /// </summary>
        /// <param name="input">current InputState of the system</param>
        public override void HandleInput(InputState input)
        {
            //Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            //Gets input states to run from
            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            KeyboardState lastKeyboardState = input.LastKeyboardStates[playerIndex];

            //Get all keys hit from the keyboard
            Keys[] pressedKeys = keyboardState.GetPressedKeys();

            //Input all keys hit into the username string
            foreach (Keys key in pressedKeys)
            {
                if (lastKeyboardState.IsKeyUp(key))
                {
                    if (key == Keys.Back && username.Length > 0) // overflows
                        username = username.Remove(username.Length - 1, 1);
                    else
                        if (key == Keys.Space)
                            username = username.Insert(username.Length, " ");
                        else
                            if (key.ToString().Length == 1)
                                username += key.ToString();
                }
            }

            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            if (keyboardState.IsKeyDown(Keys.Escape) ||
                keyboardState.IsKeyDown(Keys.Enter) ||
                gamePadState.IsButtonDown(Buttons.A) ||
                gamePadState.IsButtonDown(Buttons.Start))
            {
                //Fade out music
                ScreenManager.Audio.Stop(musicCue);
                LoadingScreen.Load(ScreenManager, true, ControllingPlayer,
                                                           new TheGame(levelNum, konamiCode, username));
            }
        }
    }
}