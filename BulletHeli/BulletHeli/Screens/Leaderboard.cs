﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endregion //USING

namespace BulletHeli
{
    public class Leaderboard : GameScreen
    {
        #region PROPERTIES

        //Managers and such
        private ContentManager content;
        private SpriteBatch spriteBatch;
        private GraphicsDevice graphicsDevice;

        //Properties
        private float pauseAlpha;                   //Used for sweet fade in
        private Vector2 topCenter;                  //Top center of the screen, used to draw stuff
        private SpriteFont headerFont;              //Huge bold font for header
        private SpriteFont font;                    //Normal game font for the actual leaderboard
        private string musicCue = "LeaderboardMusic";//Name of the music cue we are playing on this screen

        //Text!
        String header;                              //Header string
        String[] names = new String[10];            //Array of all usernames in leaderboard
        String[] scores = new String[10];           //Array of all scores in leaderboard
        Vector2 headerPosition;                     //Positions of above
        Vector2[] namesPosition = new Vector2[10];
        Vector2[] scoresPosition = new Vector2[10];

        #endregion //PROPERTIES

        /// <summary>
        /// Creates a new leaderboard screen
        /// </summary>
        public Leaderboard()
        {
            //GameScreenManager setup
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        /// <summary>
        /// Initializes the new leaderboard screen
        /// </summary>
        private void Initialize()
        {
            //Save system references
            graphicsDevice = ScreenManager.GraphicsDevice;

            //Set up the top center of the screen
            topCenter = new Vector2(ScreenManager.GraphicsDevice.Viewport.Width / 2, 0);

            //Load font
            headerFont = content.Load<SpriteFont>("Fonts/GameFont");
            font = content.Load<SpriteFont>("UI/TheFont");

            //Header?!
            header = "Leaderboard";
            headerPosition = headerFont.MeasureString(this.header);
            headerPosition = new Vector2(topCenter.X - (headerPosition.X / 2), 20);
            float columnPosition = 100;

            //Get leaderboard from the internet (or local)
            string[][] data = LeaderboardData.Get();

            //Setup names and scores
            for (int i = 0; i < 10; i++)
            {
                names[i] = data[i][0];
                namesPosition[i] = new Vector2(topCenter.X - 300, columnPosition);
                scores[i] = data[i][1];
                scoresPosition[i] = new Vector2(topCenter.X + 300, columnPosition);
                columnPosition += 40;
            }
        }

        /// <summary>
        /// Loads any content required within the leaderboard screen
        /// 
        /// Also initializes the leaderboard since GSM doesn't support Initialize
        /// Plays donuts song.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            //Initialize the game
            Initialize();

            //Play the donuts song!
            ScreenManager.Audio.Play(musicCue);
        }

        /// <summary>
        /// Unloads all Leaderboard content when it is no longer needed
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }

        /// <summary>
        /// Update everything required in leaderboard
        /// </summary>
        /// <param name="gameTime">Current Game Time TimeSpan values</param>
        /// <param name="otherScreenHasFocus">Detects if this screen has user focus</param>
        /// <param name="coveredByOtherScreen">Detects if this screen is covered by another screen</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            //Update game screen
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);
        }

        /// <summary>
        /// Draw everything in leaderboard
        /// </summary>
        /// <param name="gameTime">Current Game Time TimeSpan values</param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();

            //Draw header
            spriteBatch.DrawString(headerFont, header, headerPosition, Color.White);

            //Draw all names
            for (int i = 0; i < 10; i++)
            {
                spriteBatch.DrawString(font, names[i], namesPosition[i], Color.White);
                spriteBatch.DrawString(font, scores[i], scoresPosition[i], Color.White);
            }

            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        /*******************************************************************************************************************************/

        /// <summary>
        /// Uses GSM method to handle input controls
        /// </summary>
        /// <param name="input">current InputState of the system</param>
        public override void HandleInput(InputState input)
        {
            //Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            //Gets input states to run from
            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            if (keyboardState.IsKeyDown(Keys.Escape) ||
                keyboardState.IsKeyDown(Keys.Enter) ||
                gamePadState.IsButtonDown(Buttons.A) ||
                gamePadState.IsButtonDown(Buttons.Start))
            {
                //Fade out music
                ScreenManager.Audio.Stop(musicCue);
                LoadingScreen.Load(ScreenManager, true, null, new BackgroundScreen(),
                                                           new MainMenuScreen());
            }
        }
    }
}