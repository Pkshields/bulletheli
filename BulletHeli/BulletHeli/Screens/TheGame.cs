﻿/****************************************************************************************************/
/*                                              This                                                */
/*                                               is                                                 */
/*                                           BulletHeli!                                            */
/*                                                                                                  */
/* ChangeLog: https://docs.google.com/document/d/16NV17eUaX-Wan33wYbVelAokqd4fXvBFSw0PAW5TW-U/edit  */
/* Todo: https://docs.google.com/document/d/1EDCRTcx6UQCQhLRh1BB0y9uF17WJRB2F941ZC9psnh8/edit       */
/* (If anyone needs edit access to these, let me know you Google account)                           */
/****************************************************************************************************/

#region USING
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public class TheGame : GameScreen
    {
        #region PROPERTIES

        //NEW!: Save some references that were a given in the non-GameStateManager version of the game
        ContentManager content;
        SpriteBatch spriteBatch;
        GraphicsDevice graphicsDevice;
        float pauseAlpha;
        int levelNum;

        //Game screen dimensions, in one easy to use package!
        private Rectangle gameDimensions;

        //Collision manager
        private Collision collision = new Collision();

        //Powerup manager
        Powerup powerup;

        //Attack Manager
        private AttackManger attackManager = new AttackManger();

        //Objects on screen
        private Player player1;
        //private Player player2;
        private ScrollingBackground background;
        private Texture2D sidebar;

        //Enemy Spawn setup
        private EnemyManager enemies;

        //Audio manager setup
        private AudioManager audio;
        private string musicCue = "GameMusic";

        //Managers
        private UI ui;
        private ParticleManager particle;

        //Store the Konami Code for wherever it is needed for funsies
        bool konamiCode;

        //Store the username for leaderboard submission
        private string username;

        #endregion //PROPERTIES

        #region RETURNS

        //Return Content Manager
        public ContentManager ContentManager
        { get { return content; } }

        //Return Sprite Batch
        public SpriteBatch SpriteBatch
        { get { return spriteBatch; } }

        //Return Graphics Device
        public GraphicsDevice GraphicsDevice
        { get { return graphicsDevice; } }

        //Return the screen dimensions
        public Rectangle GameDimensions
        { get { return gameDimensions; } }

        //Return the collision manager
        public Collision Collision
        { get { return collision; } }

        //Return the powerup manager
        public Powerup Powerup
        { get { return powerup; } }

        public AttackManger Attack
        { get { return attackManager; } }

        //Return the player
        public Player Player
        { get { return player1; } }

        //Return the audio manager
        public AudioManager Audio
        { get { return audio; } }

        //Return the name of the music cue to play
        public string MusicCue
        { get { return musicCue; } }

        //Return the name of the music cue to play
        public ParticleManager ParticleManager
        { get { return particle; } }

        //Return a generic font
        public SpriteFont GenericFont
        { get { return ScreenManager.Font; } }

        //Return the enemy manager
        public EnemyManager EnemyManager
        { get { return enemies; } }

        //Return the Konami Code check for whoever could need such a thing
        public bool KonamiCode
        { get { return konamiCode; } }

        //Return the username that the player entered
        public string Username
        { get { return username; } }

        #endregion //RETURNS

        /// <summary>
        /// Initilasie a copy of a BulletHeli level screen
        /// </summary>
        /// <param name="konamiCode">Bool stating if the user has entered the Konami Code</param>
        /// <param name="username">Username to submit to leaderboard on end</param>
        public TheGame(int levelNum, bool konamiCode, string username)
        {
            //In case the player entered the Konami Code, have some fun with them
            this.levelNum = levelNum;
            this.konamiCode = konamiCode;
            this.username = username;

            //GameScreenManager setup
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        /// <summary>
        /// Initialize the inital values within The Game
        /// </summary>
        private void Initialize()
        {
            //Save system references
            graphicsDevice = ScreenManager.GraphicsDevice;

            //Set up game dimensions
            gameDimensions = new Rectangle(140, 0, ScreenManager.GraphicsDevice.Viewport.Width - 140, ScreenManager.GraphicsDevice.Viewport.Height);

            //STOP EVERYTHING. Audio. Get from new GameScreen API
            audio = ScreenManager.Audio;

            //Initalise the players
            player1 = new Player();
            player1.Initialize(this, 1, 100, new Vector2((ScreenManager.GraphicsDevice.Viewport.Width - player1.Width) / 2, ScreenManager.GraphicsDevice.Viewport.Height - player1.Height));
            //player2 = new Player();
            //player2.Initialize(this, 2, new Vector2((GraphicsDevice.Viewport.Width - player2.Width) / 2, GraphicsDevice.Viewport.Height - player2.Height));

            //Initalise scrolling background
            background = new ScrollingBackground();
            background.Initialize(this, true, 3);

            //Initalise the enemy spawn
            enemies = new EnemyManager();
            enemies.Initialize(this, levelNum);

            //Initalise the UI
            ui = new UI();
            ui.Initialize(this);

            //Initalise the powerup manager
            powerup = new Powerup();
            powerup.Initialize(this);

            //Initialize the Particle effect manager
            particle = new ParticleManager();
            particle.Initialize(this);
            //Add particle types to the manager
            particle.AddType(20, 75, 5, 20, 0.5f, 1.5f, 0.6f, 1f, null, null, null, null);
            particle.AddType(20, 40, 7, 15, 0.3f, 0.6f, 0.2f, 0.4f, null, null, null, null);
            particle.AddType(100, 200, -10, -30, 1f, 3f, 1f, 2f, 0, 0, 0, 0);
        }

        /// <summary>
        /// Load all content not already loaded anywhere else in the initialize code
        /// 
        /// Also runs the Initialize method since it is unsupported in GSM
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            
            //Initialize the game
            Initialize();

            //Load the player sprites
            player1.LoadContent();
            //player2.LoadContent();

            //Load the BG sprite
            //background.LoadContent();     //TODO: Content loaded early for calculation purposes

            //Load enemy sprites
            //enemies.LoadContent();

            //Load powerup sprites
            powerup.LoadContent();

            //Load sidebar into memory
            sidebar = ContentManager.Load<Texture2D>("Background/Sidebar");

            //Load UI content
            ui.LoadContent();

            //Load particle effects content
            particle.LoadContent();

            //Start playing the game music
            audio.Play(musicCue);

            //When the content loading has finished, restart the games time
            ScreenManager.Game.ResetElapsedTime();
        }

        /// <summary>
        /// Unload all content on game screen dissappearing
        /// </summary>
        public override void UnloadContent()
        {
            ContentManager.Unload();
        }

        /// <summary>
        /// Update everything required in game
        /// </summary>
        /// <param name="gameTime">Current Game Time TimeSpan values</param>
        /// <param name="otherScreenHasFocus">Detects if this screen has user focus</param>
        /// <param name="coveredByOtherScreen">Detects if this screen is covered by another screen</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            //Update game screen
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            //If the game is active (I.E. not paused or anything)
            if (IsActive)
            {
                //What it says
                player1.Update(gameTime);
                //player2.Update(gameTime);
                background.Update();
                enemies.Update(gameTime);
                attackManager.Update(gameTime);

                //Update UI if necessary
                ui.Update(gameTime);

                //Update the powerups
                powerup.Update();
                particle.Update(gameTime);

                //Run the collision methods
                collision.Check();
                collision.Cleanup();
            }
        }

        /// <summary>
        /// Draw everything in the game, usually by calling draw methods
        /// </summary>
        /// <param name="gameTime">Current Game Time TimeSpan values</param>
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch = ScreenManager.SpriteBatch;

            //Draw everything part 1
            spriteBatch.Begin();
            background.Draw(spriteBatch);   //Drawing BG
            spriteBatch.End();

            //Objects that need its own spritebatch
            particle.Draw();

            //Draw everything part 2
            spriteBatch.Begin();
            attackManager.Draw(spriteBatch);
            player1.Draw(spriteBatch);      //Draw player1
            //player2.Draw(spriteBatch);    //Draw player2
            enemies.Draw(spriteBatch);      //Draw temp enemies
            powerup.Draw();                 //Draw powerups
            SpriteBatch.Draw(sidebar, Vector2.Zero, Color.White);//Draw sidebar
            ui.Draw(spriteBatch);           //Draw UI
            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        /*******************************************************************************************************************************/

        /// <summary>
        /// Uses GSM method to handle input controls
        /// </summary>
        /// <param name="input">current InputState of the system</param>
        public override void HandleInput(InputState input)
        {
            //Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            //Gets input states to run from
            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];
            KeyboardState lastKeyboardState = input.LastKeyboardStates[playerIndex];
            GamePadState lastGamePadState = input.LastGamePadStates[playerIndex];

            //Check if the gamepad has been disconnected (if originally connected)
            bool gamePadDisconnected = !gamePadState.IsConnected && input.GamePadWasConnected[playerIndex];

            //Check if the player has paused the game and if so, pause
            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected)
            {
                ScreenManager.AddScreen(new PauseMenuScreen(musicCue), ControllingPlayer);
            }

            player1.Move(playerIndex, keyboardState, lastKeyboardState, gamePadState, lastGamePadState);
        }


        private Random random = new Random();
        /// <summary>
        /// Generates a random number between 2 values
        /// </summary>
        /// <param name="min">Minimum value within range</param>
        /// <param name="max">Maximum value within range</param>
        /// <returns></returns>
        public float RandomBetween(float min, float max)
        {
            return min + (float)random.NextDouble() * (max - min);
        }
    }
}