﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class LeaderboardData
    {
                                                #region PARSING

        //http://www.csharp-station.com/HowTo/HttpWebFetch.aspx
        private static string GetWeb(string url)
        {
            // used to build entire input
            StringBuilder sb = new StringBuilder();

            // used on each read operation
            byte[] buf = new byte[8192];

            // prepare the web page we will be asking for
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);

            // execute the request
            HttpWebResponse response = (HttpWebResponse) request.GetResponse();

            // we will read data via the response stream
            Stream resStream = response.GetResponseStream();

            string tempString = null;
            int count = 0;

            do
            {
                // fill the buffer with data
                count = resStream.Read(buf, 0, buf.Length);

                // make sure we read some data
                if (count != 0)
                {
                    // translate from bytes to ASCII text
                    tempString = Encoding.ASCII.GetString(buf, 0, count);

                    // continue building the string
                    sb.Append(tempString);
                }
            }
            while (count > 0); // any more data to read?

            // print out page source
            return sb.ToString();
        }

        private static string[][] ParseString (string data)
        {
            //Parse data into separate lines
            string[] lines = data.Split('&');

            //Parse these into their separate components in one big loop
            int count = lines.Count();
            string[][] parsedData = new string[count][];
            for (int i = 0; i < count; i++)
            {
                parsedData[i] = lines[i].Split('|');
            }

            //return value to whomever needs it (I guess probably leaderboard?)
            return parsedData;
        }

                                                #endregion //PARSING

                                                #region GET/SET

        public static string[][] Get()
        {
            //Store raw leaderboard content
            String data;

            try
            {
                //Get data from server
                data = GetWeb("http://muzene.com:80/uni/bulletheli/getlb.php");
            }
            catch
            {
                //If unable to connect to server
                StreamReader sr = new StreamReader("Content/Level/Level1Leaderboard.txt");
                data = sr.ReadLine();
            }

            //Return parsed leaderboard
            return ParseString(data);

        }

        //http://stackoverflow.com/questions/5747045/call-php-from-c-sharp-for-windows-7-mobile
        public static void Set(string username, string score)
        {
            //Set data to send
            string data = "Username=" + username + "&Score=" + score;
            

            //Send data into aether (?)
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://muzene.com:80/uni/bulletheli/setlb.php?" + data);
                request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
                HttpWebResponse requestResponse = (HttpWebResponse)request.GetResponse();
            }
            catch
            {
                //Catch any exceptions and take it as offline leaderboard access
                //Get offline leaderboard from content
                StreamReader sr = new StreamReader(@"Content/Level/Level1Leaderboard.txt");
                string offlineLeaderboard = sr.ReadLine();
                sr.Close();

                //Parse it
                string[][] parsedLeaderboard = ParseString(offlineLeaderboard);

                //New leaderboard to build, adding new score in the correct position
                string[][] newLeaderboard = new string[parsedLeaderboard.Count()][];

                //loop through array in reverse order
                bool hasNewScore = false;
                for (int i = 0; i < parsedLeaderboard.Count(); i++)
                {
                    //If the new high score hasn't been inserted yet
                    if (!hasNewScore)
                    {
                        //If the leaderboard score is higher, add it to new leaderbpard ahead of new score
                        if (Convert.ToInt32(parsedLeaderboard[i][1]) > Convert.ToInt32(score))
                        {
                            newLeaderboard[i] = parsedLeaderboard[i];
                        }
                            //If the score is less, add new score instead and break
                        else
                        {
                            string[] newScoreArray = {username, score};
                            newLeaderboard[i] = newScoreArray;
                            hasNewScore = true;                 //Switch to alternate path (adding old scores below this new one)
                        }
                    }
                    else
                    {
                        //If it has, insert the rest of the old scores below it
                        newLeaderboard[i] = parsedLeaderboard[i-1];
                    }
                }

                //Build string for saving locally
                string newLBString = "";
                for (int i = 0; i < newLeaderboard.Count(); i++)
                {
                    newLBString += newLeaderboard[i][0] + "|" + newLeaderboard[i][1] + "&";
                }

                //Remove last char - unnecessary
                newLBString = newLBString.Remove(newLBString.Length-1);

                //Write new offline leaderboard to file
                StreamWriter sw = new StreamWriter("Content/Level/Level1Leaderboard.txt");
                sw.WriteLine(newLBString);
                sw.Close();
            }
        }
                                                #endregion //GET/SET
    }
}
