﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public class Collision
    {
                                                #region COLLISION LIST

        /// <summary>
        /// Two lists, one which holds all currently collidable objects and another that holds objects waiting to be removed.
        /// </summary>
        private List<CollisionObject> collList = new List<CollisionObject>();
        private List<CollisionObject> collRemoveList = new List<CollisionObject>();

        //Add obejct to list
        public void AddObj (CollisionObject value)
        { collList.Add(value); }

        //Add object to list of objects to remove
        public void RemoveObj(CollisionObject value)
        { collRemoveList.Add(value); }

                                                #endregion //COLLISION LIST

        #region COLLISION CHECK

        /// <summary>
        /// Method to check all possible collisions
        /// 
        /// Checks everything in the collision list with everything. If a collision is detected (and isn't colliding with itself) 
        /// It sends a call to both objects, with a reference to the colliding object for each object to handle its own consequences
        /// </summary>
        public void Check()
        {
            //Loop through entire list twice
            for (int i = 0; i < collList.Count; i++)
                for (int j = 0; j < collList.Count; j++)

                    //If the objects aren't the same and:
                    //Check if i is less than j, avoids duplocation and collision being aplied twice
                    if (collList[i] != collList[j] && i < j)
                    {
                        //Check collision
                        if (collList[i].Rect.Intersects(collList[j].Rect))
                        {
                            //If objects have collided, send a call to both objects to see if any action is necessary
                            collList[i].OnCollision(collList[j]);
                            collList[j].OnCollision(collList[i]);
                        }
                    }
        }

        #endregion //COLLISION CHECK

        #region COLLISION CLEANUP

        /// <summary>
        /// Removes all removed items from their respective lists
        /// Q: Why not removed immediately? 
        /// A: Each object handles their own collisions. So if a bullet hits an enemy, bullet checks collision, detects collision
        ///     and removes itself. Then when enemy checks collision, the bullet has been removed, so it doesn't detect any collision
        /// </summary>
        public void Cleanup()
        {
            //Player
            foreach (var item in collRemoveList)
                collList.Remove(item);
            collRemoveList.Clear();
        }

                                                #endregion //COLLISION CLEANUP

    }
}

