﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class ScrollingBackground
    {
        #region VARIABLES

        //Variables
        private Texture2D bgTexture;    //Background texture to loop
        private Vector2[] hellaBG;      //List of current sprite locations

        //Settings
        private int speedX = 0;         //Speed Bg is scrolling at horizontally
        private int speedY = 0;         //Speed Bg is scrolling at vertically
        private bool vertical;

        private TheGame game;           //Link to game

        #endregion //VARIABLES

        #region INITIALIZE

        /// <summary>
        /// Initalises new background and generates number of sprites needed for seemless scrolling
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="viewHeight"></param>
        public void Initialize(TheGame game, bool _vertical, int _speed)
        {
            //Save TheGame for referencing
            this.game = game;

            //Save settings
            vertical = _vertical;

            //Load content early for calculation purposes
            LoadContent();

            //Save settings for specific directional purposes
            int directionX, directionY;
            if (vertical)
            {
                speedY = _speed;
                directionY = speedY / Math.Abs(speedY);                     //Check which direction the background is scrolling
                directionX = 0;
                hellaBG = new Vector2[((int)game.GameDimensions.Height / bgTexture.Height) + 1]; //Set up the array for the number of textures needed for smooth scroll
            }
            else
            {
                speedX = _speed;
                directionX = speedX / Math.Abs(speedX);                       //Check which direction the background is scrolling
                directionY = 0;
                hellaBG = new Vector2[((int)game.GameDimensions.Height / bgTexture.Width) + 1];  //Set up the array for the number of textures needed for smooth scroll
            }

            if (hellaBG.Length < 2) hellaBG = new Vector2[2]; //Bugfix - TODO: Temp?

            //Add exact positions for each texture on screen, keeping it gapless
            for (int i = 0; i < hellaBG.Length; i++)
            {
                hellaBG[i] = new Vector2(-1 * directionX * (bgTexture.Width * i), -1 * directionY * (bgTexture.Height * i));
            }
        }

        public void LoadContent()
        {
            //Load the sprite
            bgTexture = game.ContentManager.Load<Texture2D>("Background/canyon");
        }

        #endregion //INITIALIZE

        #region UPDATE

        /// <summary>
        /// Updates the position of all background sprites for next refresh
        /// </summary>
        public void Update()
        {
            //For all the sprites in the array
            for (int i = 0; i < hellaBG.Length; i++)
            {
                //Move down via the speed set
                hellaBG[i] = new Vector2(hellaBG[i].X + speedX, hellaBG[i].Y + speedY);

                //When the bg sprite is out of site, loop around for constant bg flow
                if (vertical)
                {
                    int direction = speedY / Math.Abs(speedY);  //Get direction of movement (forward or backwards)

                    //When this sprite reaches the bounds set by the dimensions of the sprite itself, restart loop
                    if (direction < 0 && hellaBG[i].Y <= -bgTexture.Height || direction > 0 && hellaBG[i].Y >= bgTexture.Height)
                    {
                        hellaBG[i].Y = hellaBG[i].Y + (bgTexture.Height * hellaBG.Length * direction * -1);
                    }
                }
                else
                {
                    int direction = speedX / Math.Abs(speedX);  //Get direction of movement (forward or backwards)

                    //When this sprite reaches the bounds set by the dimensions of the sprite itself, restart loop
                    if (direction < 0 && hellaBG[i].X <= -bgTexture.Width || direction > 0 && hellaBG[i].X >= bgTexture.Width)
                    {
                        hellaBG[i].X = hellaBG[i].X + (bgTexture.Width * hellaBG.Length * direction * -1);
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //Draw all sprites in array
            for (int i = 0; i < hellaBG.Length; i++)
            {
                spriteBatch.Draw(bgTexture, hellaBG[i], bgTexture.Bounds, Color.White);
            }
        }

        #endregion //UPDATE
    }
}
