﻿//Based of ParticleSample off App Hub

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public class ParticleManager
    {
                                                #region STRUCTS

        //Struct used to store the general settings of a type of particle
        public struct ParticleSettings
        {
            public float minVel, maxVel;
            public float minAcc, maxAcc;
            public float minlife, maxlife;
            public float minScale, maxScale;
            public float? minRotSpeed, maxRotSpeed;
            public float? minInitRot, maxInitRot;
        }

        //Struct used to pass on the specific particle settings to each separate particles
        public struct NewParticleSettings
        {
            public Vector2 direction;
            public float velocity;
            public float acceleration;
            public float lifetime;
            public float scale;
            public float rotationSpeed;
            public float initalRotation;
        }

                                                #endregion //STRUCTS

                                                #region PREFERENCES

        //References
        private TheGame game;                                                               //Save reference to the game
        Queue<Particle> freeParticles = new Queue<Particle>();                              //Queue of currently inactive particles
        private List<Particle> usedParticles = new List<Particle>();                        //List of currently used particles/active
        private List<Texture2D> textureList = new List<Texture2D>();                        //List of available to use textures
        private List<ParticleSettings> particleSettings = new List<ParticleSettings>();     //list of available particle settings



                                                #endregion //PREFERENCES

                                                #region INITIALIZATION

        public void Initialize(TheGame game)
        {
            //Save TheGame for referencing
            this.game = game;
        }
        
        public void LoadContent()
        {
            //Load the smoke, explosion and spark sprites
            textureList.Add(game.ContentManager.Load<Texture2D>("ParticleEffects/Smoke"));
            textureList.Add(game.ContentManager.Load<Texture2D>("ParticleEffects/Explosion"));
            textureList.Add(game.ContentManager.Load<Texture2D>("ParticleEffects/Pixel"));
        }
                                                #endregion //INITIALIZATION

                                                #region UPDATE

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < usedParticles.Count; i++)
            {
                //Check if particle is deactivated
                if (!usedParticles[i].Active)
                {
                    //If yes, remove from active list and add to deactive queue
                    freeParticles.Enqueue(usedParticles[i]);
                    usedParticles.RemoveAt(i);
                }
            }

            foreach (Particle particle in usedParticles)
            {
                //Update all particles
                particle.Update(gameTime);
            }
        }

        public void Draw()
        {
            //Use it's own spritebach so that we can use Additive BlendState
            game.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive);
            
            //Draw all particles);
            foreach (var particle in usedParticles)
            {
                 particle.Draw();
            }
            game.SpriteBatch.End();
        }


                                                #endregion //UPDATE

                                                #region GENERATE PARTICLES

        public void GenerateParticles(Vector2 position, int[] partType, int[] partNum)
        {
            //Generate particles.

            //If partType and partNum line up:
            if (partType.Length == partNum.Length)
                //Spawn all the particles asked for in partType
                for (int i = 0; i < partType.Length; i++)
                {
                    //Spawn all individual particles in partNum
                    for (int j = 0; j < partNum[i]; j++)
                    {
                        //Firstly, generate the new settings for this particle
                        NewParticleSettings moreNewSettings = GenerateSettings(i);

                        //See if we can take a particle from queue
                        if (freeParticles.Count > 0)
                        {
                            //If yes, reinitialise it and removve from free queue
                            Particle thisParticle = freeParticles.Dequeue();
                            thisParticle.Initialize(game, textureList[partType[i]], position,
                                                    moreNewSettings.velocity*moreNewSettings.direction,
                                                    moreNewSettings.acceleration*moreNewSettings.direction,
                                                    moreNewSettings.lifetime, moreNewSettings.scale,
                                                    moreNewSettings.initalRotation, moreNewSettings.rotationSpeed);
                            usedParticles.Add(thisParticle);
                        }

                        //Else, generate a new one
                        else
                        {
                            Particle thisParticle = new Particle();
                            thisParticle.Initialize(game, textureList[partType[i]], position,
                                                    moreNewSettings.velocity*moreNewSettings.direction,
                                                    moreNewSettings.acceleration*moreNewSettings.direction,
                                                    moreNewSettings.lifetime, moreNewSettings.scale,
                                                    moreNewSettings.initalRotation, moreNewSettings.rotationSpeed);
                            usedParticles.Add(thisParticle);
                        }
                    }
                }
        }

        private NewParticleSettings GenerateSettings(int type)
        {
            //Get the general settings from list to calculate specific particles off
            ParticleSettings getSettings = particleSettings[type];

            //Generate new specific settings and calculate all settings off the general ones
            NewParticleSettings newSettings = new NewParticleSettings();
            float angle = game.RandomBetween(0, MathHelper.TwoPi);
            newSettings.direction = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
            newSettings.velocity = game.RandomBetween(getSettings.minVel, getSettings.maxVel);
            newSettings.acceleration = game.RandomBetween(getSettings.minAcc, getSettings.maxAcc);
            newSettings.lifetime = game.RandomBetween(getSettings.minlife, getSettings.maxlife);
            newSettings.scale = game.RandomBetween(getSettings.minScale, getSettings.maxScale);

            //Since the rotation can be set to null, it can take the default vaules defined below
            if (getSettings.minRotSpeed != null && getSettings.maxRotSpeed != null) newSettings.rotationSpeed = game.RandomBetween((float)getSettings.minRotSpeed, (float)getSettings.maxRotSpeed);
            else newSettings.rotationSpeed = game.RandomBetween(-MathHelper.PiOver4, MathHelper.PiOver4);
            if (getSettings.minInitRot != null && getSettings.maxInitRot != null) newSettings.initalRotation = game.RandomBetween((float)getSettings.minInitRot, (float)getSettings.maxInitRot);
            else newSettings.initalRotation = game.RandomBetween(-MathHelper.PiOver4, MathHelper.PiOver4);

            //Return the new settings to wherever it is called from
            return newSettings;
        }

                                                #endregion //GENERATE PARTICLES

                                                #region LIST MANAGEMENT

        public void AddType(float minVel, float maxVel, 
                             float minAcc, float maxAcc, 
                             float minlife, float maxlife, 
                             float minScale, float maxScale, 
                             float? minRotSpeed, float? maxRotSpeed, 
                             float? minInitRot, float? maxInitRot)
        {
            //Adds a new general type of particles to the general list
            ParticleSettings newSettings = new ParticleSettings();

            newSettings.minVel = minVel;
            newSettings.maxVel = maxVel;
            newSettings.minAcc = minAcc;
            newSettings.maxAcc = maxAcc;
            newSettings.minlife = minlife;
            newSettings.maxlife = maxlife;
            newSettings.minScale = minScale;
            newSettings.maxScale = maxScale;
            newSettings.minRotSpeed = minRotSpeed;
            newSettings.maxRotSpeed = maxRotSpeed;
            newSettings.minInitRot = minInitRot;
            newSettings.maxInitRot = maxInitRot;

            particleSettings.Add(newSettings);
        }

                                                #endregion //LIST MANAGEMENT
    }
}