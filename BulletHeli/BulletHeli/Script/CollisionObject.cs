﻿//Note: Visual hitbox code from: http://blog.dreasgrech.com/2010/08/drawing-lines-and-rectangles-in-xna.html

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public class CollisionObject
    {

                                                #region PROPERTIES

        private TheGame game;               //Save reference to the game
        private Rectangle collBox;          //Collision rect of object
        private Vector2 offset;             //Offset for the bounding box
        private Collision collision;        //Link to current collision manager
        private Texture2D pixel;            //Box to draw in case bug testing is needed for hitbox

        private Type type;

                                                #endregion //PROPERTIES

                                                #region RETURNS 

        /// <summary>
        /// Allow read access to collBox and type for Collision manager
        /// </summary>
        public Rectangle Rect
        {
            get { return collBox; }
        }

        public Collision Collision
        {
            get { return collision; }
        }

        public Type Type
        {
            get { return type; }
        }

        public int ObjWidth
        {
            get { return collBox.Width; }
        }

        public int ObjHeight
        {
            get { return collBox.Height; }
        }

        public Vector2 ObjPosition
        {
            get { return new Vector2(collBox.X, collBox.Y); }
        }

                                                #endregion //RETURNS

                                                #region INITIALIZATION

        public void Initialize(TheGame game, Rectangle collBox, Collision collision, Type type, Vector2? offset)
        {
            //Set collision box, type and add this to collision manager
            this.collBox = collBox;
            this.collision = collision;
            if (offset != null) this.offset = (Vector2)offset;
            this.type = type;

            //Save reference to the game
            this.game = game;

            //Add to collision manager
            game.Collision.AddObj(this);

            //Set up 1x1 box to draw (and scale up) for testing where the bounding box is
            pixel = new Texture2D(game.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            pixel.SetData(new[] { Color.White });
        }

                                                #endregion //INITIALIZATION

                                                #region UPDATE

        public void Update(Vector2 newLoc)
        {
            //Updates the location of the object
            collBox.X = (int)(newLoc.X + offset.X);
            collBox.Y = (int)(newLoc.Y + offset.Y);
        }

        public void Update(Rectangle collBox)
        {
            //Updates the location of the object
            this.collBox = collBox;
        }

        public void Draw()
        {
            //If called, draw visual bounding box
            game.SpriteBatch.Draw(pixel, collBox, Color.DarkGreen);
        }

                                                #endregion //UPDATE


                                                #region COLLISION MANAGEMENT

        //Id this is called instead of direct, remove from collision manager
        public void RemoveCollision()
        {
            game.Collision.RemoveObj(this);
        }

        
        //http://social.msdn.microsoft.com/forums/en-US/csharplanguage/thread/4627bacf-4894-4b83-9868-7af5a417abf7
        /// <summary>
        /// Magical method!
        /// 
        /// Virtual method here to pass on the call to the OnCollision method to whatever objects has been extended by this class
        /// 
        /// Collision calls the OnCollision method in the CollisionObject it has a reference to. We want the method called to be the
        /// method which this CollisionObject is assigned to. This virtual method can do this, with the child method overriding it.
        /// 
        /// Idea from: http://social.msdn.microsoft.com/forums/en-US/csharplanguage/thread/4627bacf-4894-4b83-9868-7af5a417abf7
        /// 
        /// PHIL: If you have any better ideas I'd love to hear them, been struggling with this for a while now
        /// </summary>
        /// <param name="obj">Object that this object has collided with</param>
        public virtual void OnCollision(CollisionObject obj)
        { }

                                                #endregion //COLLISION MANAGEMENT
    }
}
