﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class Animation
    {
        #region PROPERTIES

        //Time Variables
        private int currentTime;
        private int prevTime;
        private int timeBetween;

        //Frame information
        private int totalFrame;
        private int currentFrame;

        //Rect information
        private Vector2 viewSize;
        private Rectangle currentRect;

        //PlayerInfo
        private Texture2D playerTexture;

        //Animation info
        private bool animate;
        private bool directionalise;
        private Vector2 origin = Vector2.Zero;

        #endregion //PROPERTIES

        public Rectangle Size
        { get { return new Rectangle((int)viewSize.X, (int)viewSize.Y, (int)viewSize.X, (int)viewSize.Y); } }

        #region INITIALIZATION

        public void Initialize(int timeBetween, int totalFrame, int currentFrame, Vector2 viewSize, Vector2? origin, float rotation = 0, bool animate = true, bool directionalise = false)
        {
            //Animation settings - Should it animate and/or directionalise
            this.animate = animate;
            this.directionalise = directionalise;

            //Animation specific settings
            currentTime = 0;
            prevTime = 0;
            this.timeBetween = timeBetween;

            //Frame information
            this.totalFrame = totalFrame;
            this.currentFrame = currentFrame;

            if (origin != null)
                this.origin = (Vector2)origin;

            //Size information
            this.viewSize = viewSize;
            currentRect = new Rectangle((int)viewSize.X * currentFrame, 0, (int)viewSize.X, (int)viewSize.Y);
        }

        public void LoadContent(Texture2D playerTexture)
        {
            //Access to the player texture
            this.playerTexture = playerTexture;
        }

        #endregion //INITIALIZATION

        #region UPDATE

        /// <summary>
        /// Animates the object at the framerate it is set up update at and/or
        /// changes the direction sprite depending on wherever the object is moving
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="_direction"></param>
        public void Update(GameTime gameTime, int _direction = 0)
        {
            //If animate is enabled:
            if (animate)
            {
                //Set current time
                currentTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;

                //Check if it is time to update the animation
                if (currentTime >= prevTime + timeBetween)
                {
                    //Yes, next frame ensuring it doesn't overrun
                    currentFrame++;
                    if (currentFrame == totalFrame)
                        currentFrame = 0;

                    //Update info
                    prevTime = currentTime;
                    currentRect = new Rectangle((int)viewSize.X * currentFrame, currentRect.Y, (int)viewSize.X,
                                                (int)viewSize.Y);
                }
            }

            //If directional is enabled:
            if (directionalise)
            {
                //Move to correct row in spritesheet depending on direction
                currentRect = new Rectangle(currentRect.X, (int)viewSize.Y * _direction, currentRect.Width, currentRect.Height);
            }
        }

        public void Draw(SpriteBatch spriteBatch, Rectangle aniRect, float rotation)
        {
            //Draw sprite
            spriteBatch.Draw(playerTexture, aniRect, currentRect, Color.White, rotation, origin, SpriteEffects.None, 0f);
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 position)
        {
            //Draw sprite
            spriteBatch.Draw(playerTexture, position, currentRect, Color.White);
        }

        #endregion //UPDATE
    }
}