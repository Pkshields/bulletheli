﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public class AudioManager
    {
        // Audio objects
        private AudioEngine engine;                             //Stores the audio engine
        private SoundBank soundBank;                            //Sound Bank
        private WaveBank waveBank;                              //And Wave Bank from our game
        private List<Cue> cueBank = new List<Cue>();            //List of currently active sound cues
        private List<string> cueBankName = new List<string>();  //List used to access list above via text string

        public void Initialize()
        {
            // Initialize audio objects.
            engine = new AudioEngine("Content/Music/Music.xgs");
            soundBank = new SoundBank(engine, "Content/Music/SoundBank.xsb");
            waveBank = new WaveBank(engine, "Content/Music/WaveBank.xwb");
        }

        public void Update()
        {
            // Update the audio engine.
            engine.Update();
        }

        public void Play(string cue)
        {
            //Play the required cue
            cueBank.Add(soundBank.GetCue(cue));
            cueBankName.Add(cue);
            cueBank[cueBankName.Count-1].Play();
        }

        public void Stop(string cue)
        {
            //Using the name of the cue, get the instance of the cue from the list, stop it and get rid of it
            int cueNum = cueBankName.IndexOf(cue);
            
            cueBank[cueNum].Stop(AudioStopOptions.AsAuthored);
            cueBankName.RemoveAt(cueNum);
            cueBank.RemoveAt(cueNum);
        }
    }
}
