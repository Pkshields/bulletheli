﻿//Based of ParticleSample off App Hub

#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    class Particle
    {
                                                #region PREFERENCES

        //References
        private TheGame game;                                   //Save reference to the game

        //Particle textures
        private Texture2D particleTexture;                      //Store the texture for the particle to draw

        //Movement variables
        private Vector2 position;                               //The position the particle is currently at
        private Vector2 velocity;                               //Velocity to apply to particle
        private Vector2 acceleration;                           //Acceleration to apply to the velocity

        //Draw variables
        private float lifetime;                                 //Time the particle is alive
        private float timeSinceStart;                           //Time at which the particle started
        private float scale;                                    //Scale to draw the particle at
        private float rotation;                                 //Current rotation of the particle
        private float rotationSpeed;                            //Speed at which to rotate the particle at
        private Vector2 origin;                                 //Origin (center for particles to rotate)

        //Settings 
        private bool active;                                    //Defines if the particle is currently active

                                                #endregion //PREFERENCES

                                                #region RETURNS

        //Return if the particle is active for Particle Manager purposes
        public bool Active
        {
            get { return active; }
        }

                                                #endregion //RETURNS

                                                #region INITIALIZATION

        public void Initialize(TheGame game, Texture2D particleTexture, Vector2 position, Vector2 velocity, Vector2 acceleration, float lifetime, float scale, float rotation, float rotationSpeed)
        {
            //Save TheGame for referencing
            this.game = game;
            this.particleTexture = particleTexture;

            //Set movement variables
            this.position = position;
            this.velocity = velocity;
            this.acceleration = acceleration;

            //Set draw variables
            this.lifetime = lifetime;
            this.timeSinceStart = 0.0f;
            this.scale = scale;
            this.rotation = rotation;
            this.rotationSpeed = rotationSpeed;
            this.origin = new Vector2(particleTexture.Width/2, particleTexture.Height/2);

            //Settings
            active = true;
        }
                                                #endregion //INITIALIZATION

                                                #region UPDATE

        public void Update(GameTime gameTime)
        {
            float totalTime = (float)gameTime.ElapsedGameTime.TotalSeconds; //Get time betweens frame
            velocity += acceleration * totalTime;                           //Calculate how much the particle should move this frame
            position += velocity * totalTime;                               //calculate the new position using that velocity

            rotation += rotationSpeed * totalTime;                          //Calculate the new rotation using the time since

            timeSinceStart += totalTime;                                    //Add the time since to the total time

            //If the particle has lasted past it's lifetime, deactivate it
            if (lifetime < timeSinceStart)
            {
                active = false;
            }
        }

        public void Draw()
        {
            //Before draw, calculate the transparency of the particle so that it fades in and out of effect
            float normalizedLifetime = timeSinceStart / lifetime;
            float alpha = 4 * normalizedLifetime * (1 - normalizedLifetime);
            Color color = Color.White * alpha;

            //Draw particle using all calculated values
            game.SpriteBatch.Draw(particleTexture, position, null, color,
                rotation, origin, scale, SpriteEffects.None, 0.0f);
        }


                                                #endregion //UPDATE
    }
}
