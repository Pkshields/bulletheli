﻿#region USING
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public class EnemyManager
    {
        private TheGame game;
        private int timeSinceLastSpawn = 0;
        private int timeTillNextSpawn = 0;
        private int nextEnemyToSpawn = 0;

        private List<EnemySettings> enemySettings;
        private List<Enemy> currentEnemies;

        //Return the list of current enemys
        public List<Enemy> EnemyList
        {
            get { return currentEnemies; }
        }

        public void Initialize(TheGame game, int levelNum)
        {
            //Save TheGame for referencing
            this.game = game;

            //Initialize the lists
            currentEnemies = new List<Enemy>();
            
            //Initialize the level
            GenerateLevel(levelNum);
        }

        public void Update(GameTime gameTime)
        {
            //Update all enemys currently active
            int currentCount = currentEnemies.Count;
            for (int i = 0; i < currentCount; i++)
            {
                //Update every enemy, see what they are doing
                currentEnemies[i].Update(gameTime);

                //If enemy is not inactive, remove from updating list
                if (!currentEnemies[i].Active)
                {
                    currentEnemies.Remove(currentEnemies[i]);
                    i--;
                    currentCount--;
                }
            }

            //Update current time
            timeSinceLastSpawn += gameTime.ElapsedGameTime.Milliseconds;

            //If there are still enemies to spawn
            if (nextEnemyToSpawn < enemySettings.Count)
            {
                //If time has elapsed since last spawn
                if (timeSinceLastSpawn >= timeTillNextSpawn)
                {
                    //Using the enemy type string parsed in enemySettings, generate a new enemy of that type
                    string enemyType = enemySettings[nextEnemyToSpawn].EnemyType;
                    Enemy enemyInstance;

                    //In case there is a problem with the generation
                    try
                    {
                        enemyInstance = (Enemy) Activator.CreateInstance(Type.GetType("BulletHeli." + enemyType));
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

                    //Initialize the new enemy with the settings provided
                    enemyInstance.Initialize(game,
                                             game.ContentManager.Load<Texture2D>(enemySettings[nextEnemyToSpawn].Texture),
                                             enemySettings[nextEnemyToSpawn].NumOfFrames,
                                             new Vector2(enemySettings[nextEnemyToSpawn].PositionX,
                                                         enemySettings[nextEnemyToSpawn].PositionY),
                                             enemySettings[nextEnemyToSpawn].Health,
                                             enemySettings[nextEnemyToSpawn].EnemyArgs);

                    //Add enemy to the enemy list
                    currentEnemies.Add(enemyInstance);

                    //Run an update to get everything straight
                    enemyInstance.Update(gameTime);

                    //Increment the next enemy to spawn, reset timer and set time till next spawn
                    timeSinceLastSpawn = 0;
                    timeTillNextSpawn = enemySettings[nextEnemyToSpawn].TimeTillNextSpawn;
                    nextEnemyToSpawn++;
                }
            }

            //Else, we are at the end of spawn time. Check that there are still enemies left on screen
            else if (currentEnemies.Count == 0)
                //End the game
                game.Player.OnDeath();

            Console.WriteLine(timeSinceLastSpawn);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //Draw all currently alive enemies
            foreach (var enemy in currentEnemies)
                enemy.Draw(spriteBatch);
        }

        private void GenerateLevel(int levelNum)
        {
            //Get the XML file
            string xmlFile = @"Content/Level/Level" + levelNum + ".xml";

            //Deserialize the XML file into the EnemySettings List
            XmlSerializer deserializer = new XmlSerializer(typeof(List<EnemySettings>));
            TextReader textReader = new StreamReader(xmlFile);
            enemySettings = (List<EnemySettings>)deserializer.Deserialize(textReader);
            textReader.Close();
        }
    }
}
