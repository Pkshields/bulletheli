﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public class FollowEnemy : Enemy
    {
                                                #region PREFERENCES

        //References
        private Gun gun;

        //Properties
        private Type targetType;
        private string ammoFile;
        private string weapon;
        private int ammo;
        private TimeSpan invuln;

        //Acceleration based movement
        private Vector2 initalVelocity;                         //Inital velocity
        private Vector2 velocity;                               //Current velocity to move
        private float maxAngle;
        private float angle = 0;

                                                #endregion //PREFERENCES 

                                                #region INITIALIZATION

        protected override Rectangle OnInitialize(EnemyArgs enemyArgs)
        {            
            //Set inital movement info
            this.initalVelocity = new Vector2(enemyArgs.InitalVelocityX, enemyArgs.InitalVelocityY);
            this.maxAngle = enemyArgs.Angle;

            //Settings
            this.weapon = enemyArgs.GunWeapon;
            this.ammo = enemyArgs.GunAmmo;
            this.targetType = typeof (Player);
            this.ammoFile = "Content/Ammo/EnemyAmmo.txt";
            this.invuln = TimeSpan.Zero;

            gun = new Gun();
            gun.Initialize(game, this, targetType, ammoFile);

            //Generate smaller hitbox
            Rectangle hitbox = new Rectangle((int)(position.X - 12), (int)(position.Y - 12), 24, 24);

            //Return smaller hitbox to base class
            return hitbox;
        }

                                                #endregion //INITIALIZATION

                                                #region UPDATE

        protected override void OnUpdate(GameTime gameTime)
        {
            //
            

            //Fire gun?
            gun.Fire(weapon, ammo);
            gun.Update(gameTime);

            //Check if the enemy has left the boundaries of the screen);
            if (initalActive && (
                position.X + 72 / 2 < game.GameDimensions.X ||
                position.Y + 66/ 2 < game.GameDimensions.Y ||
                position.X - 72/ 2 > game.GameDimensions.Width ||
                position.Y - 66/ 2 > game.GameDimensions.Height))
            {
                //Deactivate
                active = false;
            }

            //Check needed to see if the enemy has been within the screen borders yet
            //Needed so the enemy can start outside of screen space
            else if (position.X > game.GameDimensions.X &&
                    position.Y > game.GameDimensions.Y &&
                    position.X < game.GameDimensions.Width &&
                    position.Y < game.GameDimensions.Height)
            {
                initalActive = true;
            }

            //If enemy has died somewhere along the line
            if (!active || health <= 0)
            {
                base.RemoveCollision();
                active = false;

                if (health <= 0)
                {
                    //Add particle effects of explosion to screen
                    game.ParticleManager.GenerateParticles(position, new[] { 0, 1 }, new[] { 6, 2 });

                    //Check and see if a new powerup should spawn
                    game.Powerup.AddPowerup(position);

                    //Add points to player
                    game.Player.Score += 150;
                }
            }
        }

                                                #endregion //UPDATE

                                                #region COLLISION MANAGEMENT

        //Apply collision if required
        public override void OnCollision(CollisionObject obj)
        {
            Type objType = obj.Type;

            //If collision is with a player
            if (objType == targetType)
            {
                //Deactivate the enemy
                active = false;
            }
            else if (objType.BaseType == typeof(Attack))
            {
                //Take the damage that the bullet applied away from the enemy health
                Attack objItem = (Attack)obj;
                if (objItem.Target == this.GetType().BaseType)
                {
                    if (objType == typeof(Laser))
                    {
                        if (invuln <= TimeSpan.Zero)
                        {
                            health -= objItem.Damage;
                            invuln = objItem.Invulnerable;
                        }
                    }
                    else
                        health -= objItem.Damage;
                }
            }
        }

                                                #endregion //COLLISION MANAGEMENT

        /*******************************************************************************************************************************/
    }
}
