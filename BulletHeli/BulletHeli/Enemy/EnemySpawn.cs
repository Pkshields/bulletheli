﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public class EnemySpawn
    {
        #region PREFERENCES

        //References
        private TheGame game;                                       //Save reference to the game

        //Lists
        private List<Array> enemyList = new List<Array>();          //List of enemies to spawn
        private List<Texture2D> textureList = new List<Texture2D>();//List of possible textures
        private List<BasicEnemy> currentList = new List<BasicEnemy>();        //List of currently alive enemies

        //Preferences
        private int nextTime = 0;                                   //Time at which the next spawn should happen
        private int nextSpawn = 0;                                  //Number of the next spawn


        #endregion //PREFERENCES

        #region RETURNS

        //Return the list of current enemys
        public List<BasicEnemy> EnemyList
        {
            get { return currentList; }
        }

        #endregion //RETURNS

        #region INITIALIZATION

        public void Initialize(TheGame game)
        {
            //Save TheGame for referencing
            this.game = game;

            //Initalise Level 1 (TEMP?)
            EnemyLevelSetup(1);
        }

        public void LoadContent()
        {
            //Initalise textureList
            EnemyTextureSetup();
        }

        #endregion //INITIALIZATION

        #region UPDATE

        public void Update(GameTime gameTime)
        {
            //Update all enemys currently active
            int currentCount = currentList.Count;
            int enemyCount = enemyList.Count;
            for (int i = 0; i < currentCount; i++)
            {
                currentList[i].Update(gameTime);

                //If enemy is not inactive, remove from updating list
                if (!currentList[i].Active)
                {
                    currentList.Remove(currentList[i]);
                    i--;
                    currentCount--;
                }
            }

            //If the timer has elapsed, spawn a new enemy
            if (gameTime.TotalGameTime.TotalMilliseconds > nextTime && nextSpawn < enemyCount)
            {
                //Get next enemy info
                string[] newEnemyInfo = (string[])enemyList[nextSpawn];
                float totalSpawn = (float)Convert.ToDouble(newEnemyInfo[0]); //<<Get the total number of enemys to spawn this spawn time

                //Spawn the numebr of enemys the above value states
                //This takes the next x lines from the text file
                for (int i = 0; i < totalSpawn; i++)
                {
                    //Get the new enemy info (in case it is different from before the for statement
                    newEnemyInfo = (string[])enemyList[nextSpawn];

                    //Set up the enemy args for BasicEnemy
                    EnemyArgs newArgs = new EnemyArgs();
                    //newArgs.InitalVelocity = new Vector2((float)Convert.ToDouble(newEnemyInfo[4]), (float)Convert.ToDouble(newEnemyInfo[5]));
                    //newArgs.acceleration = new Vector2((float)Convert.ToDouble(newEnemyInfo[6]), (float)Convert.ToDouble(newEnemyInfo[7]));
                    //newArgs.gunAmmo = Convert.ToInt32(newEnemyInfo[10]);
                    //newArgs.gunWeapon = newEnemyInfo[11];

                    //Generate the new enemy
                    BasicEnemy newEnemy = new BasicEnemy();
                    newEnemy.Initialize(game, textureList[Convert.ToInt32(newEnemyInfo[1])], 3,
                                         new Vector2((float)Convert.ToDouble(newEnemyInfo[2]), (float)Convert.ToDouble(newEnemyInfo[3])),
                                         (float)Convert.ToDouble(newEnemyInfo[9]), newArgs);

                    currentList.Add(newEnemy);

                    //Set new timer
                    nextTime = (int)gameTime.TotalGameTime.TotalMilliseconds + Convert.ToInt32(newEnemyInfo[8]);

                    //Increment nextSpawn number
                    nextSpawn++;
                }
            }
            else if (nextSpawn >= enemyCount && currentList.Count == 0)
            {
                game.Player.OnDeath();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //Draw all currently alive enemies
            foreach (var enemy in currentList)
                enemy.Draw(spriteBatch);
        }


        #endregion //UPDATE

        /*******************************************************************************************************************************/

        #region LIST SETUP

        /// <summary>
        /// Set up the enemy spawn list for Level 1
        /// Set up as parsing from a text file
        /// </summary>
        /// <param name="level">Level Number to generate</param>
        public void EnemyLevelSetup(int level)
        {
            //Clear current spawnlist
            enemyList.Clear();

            //Generate a new list for this level
            switch (level)
            {
                case 1:
                    enemyList = parseTXT("Content/Level/Level1.txt", 3);
                    break;
            }

            //Reset spawn timer
            nextTime = 0;
        }

        /// <summary>
        /// Set up all usable enemy sprites
        /// </summary>
        public void EnemyTextureSetup()
        {
            //Add all enemy sprites to the list
            textureList.Add(game.ContentManager.Load<Texture2D>("Enemy/enemy-1"));
            textureList.Add(game.ContentManager.Load<Texture2D>("Enemy/enemy-2"));
            textureList.Add(game.ContentManager.Load<Texture2D>("Enemy/enemy-3"));
        }

        #endregion //LIST SETUP

        #region PARSETXT

        /// <summary>
        /// Takes in a file name then parses it out into a list containing arrays, each array containing all the words from
        /// a line of text from the file. Option to start reading from a certain line added in to allow for formatting in the
        /// text file itself
        /// 
        /// TODO: Move this to a separate file for use by both this and Gun
        /// TODO: Figure out how to integrate the float conversion into a more general script
        /// </summary>
        /// <param name="file">File we are parsing</param>
        /// <param name="startLine">Line to start parsing from</param>
        /// <returns></returns>
        private List<Array> parseTXT(string file, int startLine)
        {
            //List we are generating
            List<Array> newList = null;

            //Open file for parsing
            using (StreamReader sr = new StreamReader(file))
            {
                String line = sr.ReadLine();
                string[] words;

                //Manually move the fine pointer to the start of the lines to parse
                for (int i = 1; i < startLine; i++)
                {
                    line = sr.ReadLine();
                }

                newList = new List<Array>();

                //Parse every line into an array, using a space as the delimiter
                while (line != null)
                {
                    words = line.Split(' ');
                    newList.Add(words);
                    line = sr.ReadLine();
                }
            }

            //Return list to gun
            return newList;
        }
        #endregion //PARSETXT
    }
}
