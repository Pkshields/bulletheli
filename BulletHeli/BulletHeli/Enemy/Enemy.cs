﻿#region USING
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
#endregion //USING

namespace BulletHeli
{
    public abstract class Enemy : CollisionObject
    {
                                                #region PREFERENCES

        //References
        protected TheGame game;                                   //Save reference to the game
        private Animation animation;                            //Save Enemy texture
        private Texture2D texture;
        private Rectangle viewableRect = Rectangle.Empty;

        //Properties
        protected bool active;                                    //Sets if the enemy is active or not
        protected bool initalActive;                              //Ability to spawn the enemy out of screen
        protected float health;                                   //Health of the enemy
        private float onCollisionDamage = 5;                    //TODO: Damage the enemy makes if it hits player

        //Acceleration based movement
        private Vector2 origin;                                 //Origin to rotate around (center of image)
        protected Vector2 position;                               //Enemy position
        protected float rotation;                                 //Angle the enemy is moving in

                                                #endregion //PREFERENCES

                                                #region RETURNS

        //Return If the enemy is active
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        //Return damage made by enemy
        public float Damage
        {
            get { return onCollisionDamage; }
        }

        //Return the current position of the enemy
        public Vector2 Position
        {
            get { return position; }
        }

                                                #endregion //RETURNS

                                                #region INITIALIZATION

        public void Initialize(TheGame game, Texture2D texture, int numOfFrames, Vector2 position, float health, EnemyArgs enemyArgs)
        {
            //Save TheGame for referencing
            this.game = game;

            //Save enemy texture
            this.texture = texture;

            //Set inital movement info
            this.position = position;

            //Calculate the viewable rectangle of the sprite
            int widthOfSprite = texture.Width / numOfFrames;
            this.viewableRect = new Rectangle((int)position.X, (int)position.Y, widthOfSprite, texture.Height);
            this.origin = new Vector2(widthOfSprite / 2, texture.Height / 2);
            
            //Set up animation
            this.animation = new Animation();
            this.animation.Initialize(25, numOfFrames, 0, new Vector2(viewableRect.Width, viewableRect.Height), origin);
            this.animation.LoadContent(texture);

            //Sets the enemy as active
            active = true;
            initalActive = false;

            //Settings
            this.health = health;

            //Initialize this specific enemy
            Rectangle hitbox = OnInitialize(enemyArgs);

            //Calculate the offset for the collision box - assuming it is spawning at center
            Vector2 offset = new Vector2(-hitbox.Width/2, -hitbox.Height/2);

            //Initalise the Collision manager
            base.Initialize(game, hitbox, game.Collision, this.GetType(), offset);
        }

                                                #endregion //INITIALIZATION

        public void Update(GameTime gameTime)
        {
            //Update this specific enemy
            OnUpdate(gameTime);

            //Update collision manager
            base.Update(position);

            //Update animation
            viewableRect.X = (int)position.X;
            viewableRect.Y = (int)position.Y;
            animation.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //Draw enemy!
            animation.Draw(spriteBatch, viewableRect, rotation);
            //base.Draw();
        }

                                                #region VIRTUAL METHODS

        protected virtual Rectangle OnInitialize(EnemyArgs enemyArgs) { return Rectangle.Empty; }
        protected virtual void OnUpdate(GameTime gameTime) { }
        //public virtual void OnCollision(CollisionObject obj) { }

                                                #endregion //VIRTUAL METHODS
    }

    /// <summary>
    /// Used to store specific settings for specific types of enemies
    /// </summary>
    public class EnemyArgs
    {
        //If gun
        public string GunWeapon;
        public int GunAmmo;
        public string AmmoFile = "Content/Ammo/EnemyAmmo.txt";

        //BasicEnemy
        public float InitalVelocityX, InitalVelocityY;
        public float AccelerationX, AccelerationY;

        //FollowEnemy
        public float Angle;
    }

    /// <summary>
    /// Used to store generic settings for parsing through with XML
    /// </summary>
    public class EnemySettings
    {
        public string EnemyType;
        public string Texture;
        public int NumOfFrames;
        public float PositionX, PositionY;
        public float Health;
        public EnemyArgs EnemyArgs;
        public int TimeTillNextSpawn;
    }
}
