﻿Number of Bullets per Shot - Angle Between - Speed - Time Between - Damage - Texture - Offset

BULLETS
1 0 8 .125 10 playerBullet 0
3 5 8 .125 10 playerBullet 0
5 8 8 .125 10 playerBullet 0
7 10 8 .125 10 playerBullet 0
8 12 8 .125 10 playerBullet 0
END

Number of Bombs per Shot - Angle Between - Speed - Time Between - Fuse - Damage - Shrapnel Type - Texture - Time Between Frames - No. of Frames - Start Frame - Frame Width - Frame Heigth - Animate - Offset

BOMBS
1 0 7 1 1 10 1 playerBomb 30 2 0 14 14 true 0
1 0 7 1 .9 10 1 playerBomb 30 2 0 14 14 true 0
1 0 7 1 0.8 10 1 playerBomb 30 2 0 14 14 true 0
2 15 7 1 0.7 10 1 playerBomb 30 2 0 14 14 true 0
3 0 7 1 0.5 10 1 playerBomb 30 2 0 14 14 true 0
END

Duration - Time Between  - Invulerable Time - Width - Damage - Texture - Number of Shots

LASERS
1 1 0.3 20 50 playerLaser
0.5 0.8 0.1 40 75 playerLaser
END

Max Acceleration - Max Speed - Arrival Radius - Slow Radius - Damage - Lock-On Delay - Time Between - Texture

MISSILES
10 50 100 150 50 0.1 1 playerMissile
20 30 100 200 100 0.1 1 playerMissile
END